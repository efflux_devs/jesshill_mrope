<!DOCTYPE html>
<html>
    <head>
        <title>Jesshill | Contact Us</title>
        <!-- custom-theme -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Oil Industry Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
        Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
                function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!-- //custom-theme -->
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link rel="stylesheet" href="css/contact.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="css/fonts/font.css">
        <link href="css/test.css" rel="stylesheet" type="text/css" media="all" />
        <!-- font-awesome-icons -->
        <link href="css/font-awesome.css" rel="stylesheet"> 
        <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
        <script src="js/bootstrap.js"></script>
    </head>
    <style>
        .mySlides {display:none;height: 600px;}
        .dropdown-menu a:hover {background-color: #f1f1f1;}
        .dropdown:hover .dropdown-menu {
            display: block;
        }    
        .dropdown-menu a {
            text-transform: uppercase;
        }
        .right_wrap {
            background: #606060;
            padding: 20px;
            color: #fff;
            font-family: opensans !important;
        }
    </style>
    <body>
        <?php
            include ("header_bottom.php");
        ?>

        <?php
            include ("header.php");
        ?>
        
        <div class="banner_wrap">
            <div class="baner"></div>
        </div>
        
        <!-- <div class="">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                  <div class="item active">
                    <img src="images/cont.jpg" alt="Los Angeles" style="width:100%;height: 380px;">
                  </div>

                  <div class="item">
                    <img src="images/contt.jpg" alt="Chicago" style="width:100%;height: 380px;">
                  </div>
        
                  <div class="item">
                    <img src="images/conttt.jpg" alt="New York" style="width:100%;height: 380px;">
                  </div>
                </div>
            </div>
        </div> -->

        <table style="width: 100%;">
            <tr>
                <th style="text-align: center; padding: 10px 0px; font-size: 40px; font-family: montserratReg; text-transform: capitalize; color: #251021;">How can we help you?</th>
            </tr>
            <tr>
                <td style="text-align: center; padding: 10px 0px; font-size: 20px; font-family: opensans; text-transform: capitalize;">
                    Drop us a line...We're glad to be of service.
                </td>
            </tr>
        </table>

        <div class="container" style="margin-bottom: 30px;">
            <div class="row">
                <div class="col-md-8">
                    <form name="registration_form" id="registration_form" class="form-horizontal" action="process_contact_form.php" method="POST">
                        <div class="form-group form_wrap">      
                           <div class="col-sm-12">
                                <label for="sur" class="lab">Full Name:</label>
                                <input type="text" class="form-control input" name="fullname" id="fullname" placeholder="Full Name" required>
                           </div>
                        </div>

                        <div class="form-group form_wrap">      
                           <div class="col-sm-12">
                                <label for="sur" class="lab">Email Address:</label>
                                <input type="email" class="form-control input" name="emailaddress" id="emailaddress" placeholder="Enter Email Address" required>
                           </div>
                        </div><!--/form-group-->

                        <div class="form-group form_wrap">      
                           <div class="col-sm-12">
                                <label for="sur" class="lab">Subject:</label>
                                <input type="text" class="form-control input" name="subject" id="subject" placeholder="Enter Subject" required>
                           </div>
                        </div><!--/form-group-->

                        <div class="form-group form_wrap">      
                           <div class="col-sm-12">
                                <label for="sur" class="lab">Message:</label> <br>
                                <textarea class="textarea" name="message" rows="5"></textarea>
                           </div>
                        </div><!--/form-group-->
                        <input type="submit" value="Send Message" class="button">
                    </form>
                </div>
                <div class="col-md-4">
                    <div class="right_wrap">
                        <div class="">
                            <table>
                                <tr>
                                    <th style="font-family: montserratReg;">
                                        ENQUIRIES:
                                    </th>
                                </tr>
                                <tr>
                                    <td class="td">
                                        For General Enquiries
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td">
                                        <i class="fa fa-envelope" style="padding-right: 10px; color: #77C53E;" aria-hidden="true"></i>
                                        info@jesshillconsulting.com
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="">
                            <table>
                                <tr>
                                    <th style="font-family: montserratReg;">
                                        REGISTERED OFFICE ADDRESS:
                                    </th>
                                </tr>
                                <tr>
                                    <td class="td">
                                        <i class="fa fa-map-marker" style="padding-right: 10px; color: #77C53E;" aria-hidden="true"></i>
                                        77, Makinde Street,
                                    </td>
                                </tr>
                                <tr >
                                    <td class="td">
                                        off Murtala Mohammed International
                                    </td> <br>
                                </tr>
                                <tr >
                                    <td class="td">
                                        Airport Road,Lagos Nigeria.
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class=""> <br>
                            <table>
                                <tr>
                                    <th style="font-family: montserratReg;">
                                        Phone Numbers:
                                    </th>
                                </tr>
                                <tr>
                                    <td class="td">
                                        <i class="fa fa-phone" style="padding-right: 10px; color: #77C53E;" aria-hidden="true"></i>
                                        +234 818 555 5453
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
            include ("footer.php");
        ?>

        <script src="js/toucheffects.js"></script>
        <script>
            var myIndex = 0;
            carousel();

            function carousel() {
                var i;
                var x = document.getElementsByClassName("mySlides");
                for (i = 0; i < x.length; i++) {
                   x[i].style.display = "none";  
                }
                myIndex++;
                if (myIndex > x.length) {myIndex = 1}    
                x[myIndex-1].style.display = "block";  
                setTimeout(carousel, 3000); // Change image every 2 seconds
            }
        </script>
        <!-- <script>
            $(document).ready(function(){
                $("input").mouseenter(function(){
                    $(".button").fadeOut(6000);
                });
            });
        </script> -->
    </body>
</html>