<?php  
    require_once('connect.php');

    $error = $_GET['msg'];
?>

<!DOCTYPE html>
<html>
	<head>
		<title>jesshill | error</title>
	    <link rel="stylesheet" href="css/font/font.css">
	    <link rel="stylesheet" href="css/bootstrap.min.css">
	    <link rel="stylesheet" type="text/css" href="../css/display.css">
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	</head>
	<body class="error_body">
		<div class="error_div">
			<div class="error_icon">
				<i class="fa fa-exclamation-circle" aria-hidden="true"></i>
				<h2>UH OH! WE'RE SORRY!!!</h2>
				<p>
					<?php echo "$error"; ?>
				</p>

				<a href="#" onclick="goBack()">go back</a>
			</div>
		</div>


		<script>
			function goBack() {
			    window.history.back();
			}
		</script>
	</body>
</html>