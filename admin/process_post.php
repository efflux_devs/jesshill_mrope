<?php
  require_once('connect.php');
  if (isset($_POST) & !empty($_POST)) {
    submit($connection);
  }

  function submit($connection){
    $filename = uploadwww();

    $post_title = mysqli_real_escape_string($connection, $_POST['post_title']);
    $post_body = mysqli_real_escape_string($connection, $_POST['post_body']);
    $post_by = mysqli_real_escape_string($connection, $_POST['post_by']);
    $timestamp = date("Y-m-d H:i:s"); 
    $post_image = $filename;

    $sql = "INSERT INTO blogpost (post_title, post_body, post_image, post_by, signup_date) VALUES ('$post_title', '$post_body', '$post_image', '$post_by', '$timestamp')";
    $result = mysqli_query($connection,$sql);
    
    if ($result) {
      echo "Your post has been posted successfully.";
    }else{
      echo "Post Submission Failed.";
      if (!mysqli_query($connection))
        {
        echo("Error description: " . mysqli_error($connection));
        }
      mysqli_close($connection);
    }
  }

  function uploadwww(){     
    $target_dir = "uploads/";
    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

    if(isset($_POST["submit"])) {
      $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
      if($check !== false) {
        $uploadOk = 1;
      } else {
        $uploadOk = 0;
        echo "Please upload a file";
        die();
      }
    }

    // Check if file already exists
    if (file_exists($target_file)) {
      echo "Sorry, file already exists.";
      $uploadOk = 0;
      die();
    }

    // Check file size
    if ($_FILES["fileToUpload"]["size"] > 500000) {
      echo "Sorry, your file is too large.";
      $uploadOk = 0;
      die();
    }

    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif" ) {
      echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
      $uploadOk = 0;
      die();
    }

    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
      echo "Sorry, your file was not uploaded.";
      die();
    // if everything is ok, try to upload file
    } else {
      if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
          // echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
        return $_FILES["fileToUpload"]["name"];
      } else {
       echo "Sorry, there was an error uploading your file.";
      }
    }
  }
?>