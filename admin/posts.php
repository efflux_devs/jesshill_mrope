<?php 
	session_start();
	require_once('connect.php');
	if(!isset($_SESSION['username'])) {
		header("Location: login.php");
		die();
	}

	require_once('process_query.php');
?>
<!DOCTYPE html>
<html>
	<head>
		<title>All Posts</title>
		<script src="../js/jquery-2.1.4.min.js"></script>
		<script src="../js/bootstrap.js"></script>
		<link href="../blog/css/style.css" rel='stylesheet' type='text/css' />
		<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="../css/fonts/font.css">
	</head>
	<body>
		<nav class="navbar navbar-default">
			<div class="container-fluid">
			    <!-- Brand and toggle get grouped for better mobile display -->
			    <div class="navbar-header">
			      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			      </button>
			      <a class="navbar-brand" href="new.php">Jesshill</a>
			    </div>

			    <!-- Collect the nav links, forms, and other content for toggling -->
			    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			      <ul class="nav navbar-nav">
			        <li><a href="new.php">Create New</a></li>
			        <li class="active"><a href="posts.php">Posts</a></li>
			      </ul>
			      <ul class="nav navbar-nav navbar-right">
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Admin <span class="caret"></span></a>
			          <ul class="dropdown-menu">
			            <li><a href="logout.php">Logout</a></li>
			          </ul>
			        </li>
			      </ul>
			    </div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>

		<div class="technology">
			<div class="container">
				<div class="col-md-12 technology-left">
					<div class="tech-no">
						<!-- technology-top -->
						<?php
						    while ($row=mysqli_fetch_array($query_result,MYSQLI_ASSOC)) {
						    $id = $row['id'];
						    $title = $row['post_title'];
						    $body = $row['post_body'];
						    $image = $row['post_image'];
						    $by = $row['post_by'];
						    $date = $row['signup_date'];
						?>
						<div class="wthree">
							<div class="col-md-6 wthree-left wow fadeInDown"  data-wow-duration=".8s" data-wow-delay=".2s">
								<div class="tch-img">
									<a href="more.php?id=<?php echo "$id"; ?>">
										<div class="tch_img_div" style="background: url(../admin/uploads/<?php echo "$image"; ?>) no-repeat 0px 0px; background-size:cover; -webkit-background-size: cover; -o-background-size: cover; -ms-background-size: cover; -moz-background-size: cover; min-height: 210px;">
										</div>
									</a>
								</div>
							</div>

							<div class="col-md-6 wthree-right wow fadeInDown"  data-wow-duration=".8s" data-wow-delay=".2s" style="margin-bottom: 20px;">
								<h3>
									<a href="more.php?id=<?php echo "$id"; ?>">
										<?php echo "$title"; ?>
									</a>
								</h3>

								<h6 style="white-space: nowrap; overflow: hidden;text-overflow: ellipsis; width: 15em;">BY 
									<a href="more.php?id=<?php echo "$id"; ?>">
										<?php echo "$by"; ?> 
									</a>
									<?php echo "$date"; ?>.
								</h6>

								<p style="white-space: nowrap; overflow: hidden;text-overflow: ellipsis; ">
									<?php echo "$body"; ?>
								</p>
								
								<div class="bht1">
									<a href="edit.php?id=<?php echo "$id"; ?>">Edit</a>
									<a href="delete.php?id=<?php echo "$id"; ?>">Delete</a>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="clearfix"></div> 
						</div>
						<?php }?>
					</div>
				</div>
				<div class="clearfix"></div>
				<!-- technology-right -->
			</div>
		</div>
	</body>
</html>