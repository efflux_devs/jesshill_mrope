<?php
	session_start();
	require_once('connect.php');
	if(!isset($_SESSION['username'])) {
		header("Location: login.php");
		die();
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>New Post</title>
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<script src="../js/jquery-2.1.4.min.js"></script>
	<script src="../js/bootstrap.js"></script>
	<script src="../js/register.js"></script>
	<style>
		#form-messages{
	        color: green;
	        font-family: lato;
	        text-transform: capitalize;
	        font-weight: bold;
	        font-size: 15px;
	        text-align: center;
        }
	</style>
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="new.php">Jesshill</a>
		    </div>

		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		        <li class="active"><a href="new.php">Create New</a></li>
		        <li><a href="posts.php">Posts</a></li>
		      </ul>
		      <ul class="nav navbar-nav navbar-right">
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Admin <span class="caret"></span></a>
		          <ul class="dropdown-menu">
		            <li><a href="logout.php">Logout</a></li>
		          </ul>
		        </li>
		      </ul>
		    </div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>
	<div class="container" style="background-color: #ddd;">
	  <h2>Blog Post</h2>
	  <form class="form-horizontal" action="process_post.php" method="POST" id="blogForm" enctype="multipart/form-data">
	    <div class="form-group">
	      <label class="control-label col-sm-2">Post Title:</label>
	      <div class="col-sm-10">
	        <input type="text" class="form-control" id="post_title" placeholder="" name="post_title">
	      </div>
	    </div>
	    
	    <div class="form-group">
	      <label class="control-label col-sm-2">Post Body:</label>
	      <div class="col-sm-10">  
	      	<textarea type="text" class="form-control" placeholder="" id="post_body" name="post_body" style="width: 100%; max-width: 100%; height: 250px; max-height: 250px;"></textarea>        
	      </div>
	    </div>

	    <div class="form-group">
	      <label class="control-label col-sm-2">Choose File:</label>
	      <div class="col-sm-10">
	        <input type="file" name="fileToUpload" id="fileToUpload" accept="image/*">
	      </div>
	    </div>

	    <div class="form-group">
	      <label class="control-label col-sm-2">Post By:</label>
	      <div class="col-sm-10">
	        <input type="text" class="form-control" id="post_by" placeholder="" name="post_by">
	      </div>
	    </div>

	    <div class="form-group">        
	      <div class="col-sm-offset-2 col-sm-10">
	        <button type="submit" class="btn btn-default" id="submit" name="submit">Submit</button>
	      </div>
	    </div>
	  </form>
	  <div id="form-messages"></div>
	</div>
</body>
</html>