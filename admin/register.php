<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Sign-up | Jesshill</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
  <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.min.css'>
  <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css'>
  <link rel="stylesheet" type="text/css" href="../css/fonts/font.css">
  <link rel="stylesheet" href="../css/style_1.css">
  <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
        function hideURLbar(){ window.scrollTo(0,1); } </script>  
</head>

<body>
  <div class="signup__container">
    <div class="container__child signup__thumbnail">
      <div class="thumbnail__content text-center">
        <h1 class="heading--primary">Welcome to Jesshill.</h1>
        <h2 class="heading--secondary">Are you ready to sign up?</h2>
      </div>
      <div class="signup__overlay"></div>
    </div>

    <div class="container__child signup__form">
      <form action="process_register.php" method="POST" id="registerForm">
        <div class="form-group">
          <label for="username">Username</label>
          <input class="form-control" type="text" name="username" id="username" placeholder="Username" required />
        </div>
        <div class="form-group">
          <label for="email">Email</label>
          <input class="form-control" type="email" name="email" id="email" placeholder="example@jesshill.com" required />
        </div>
        <div class="form-group">
          <label for="password">Password</label>
          <input class="form-control" type="password" name="password" id="password" placeholder="********" required />
        </div>
        <div class="m-t-lg">
          <ul class="list-inline">
            <li>
              <input class="btn btn--form" type="submit" value="Register" id="submit" />
            </li>
            <li>
              <a class="signup__link" href="login.php">I am already a member</a>
            </li>
          </ul>
        </div>
      </form>  
    </div>
  </div>
</body>
</html>
