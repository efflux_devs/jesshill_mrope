<?php 
	require_once('../admin/process_query.php');
?>
<!DOCTYPE HTML>
<html>
<head>
	<title>Jesshill | Blog</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Style Blog Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="applijewelleryion/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
	<!-- Custom Theme files -->
	<link href='//fonts.googleapis.com/css?family=Raleway:400,600,700' rel='stylesheet' type='text/css'>
	<link href="css/style.css" rel='stylesheet' type='text/css' />	
	<link href="css/font-awesome.css" rel="stylesheet"> 
	<link rel="stylesheet" type="text/css" href="css/fonts/font.css">
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<!-- animation-effect -->
	<link href="css/animate.min.css" rel="stylesheet"> 
	<script src="js/wow.min.js"></script>
	<script>
	 new WOW().init();
	</script>
</head>

<body>
	<div class="header" id="ban">
		<div class="container">
			<div class="head-left wow fadeInLeft animated animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInLeft;">
			</div>
			<div class="header_right wow fadeInLeft animated animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInLeft;">
				<nav class="navbar navbar-default">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
						<nav class="link-effect-7" id="link-effect-7">
							<ul class="nav navbar-nav">
								<li class="active act"><a href="../index.php">VISIT MAIN WEBSITE</a></li>
							</ul>
						</nav>
					</div>
					<!-- /.navbar-collapse -->
				</nav>
			</div>
			<div class="clearfix"> </div>	
		</div>
	</div>

	
	<!--start-main-->
	<div class="header-bottom">
		<div class="container">
			<div class="logo wow fadeInDown"  data-wow-duration=".8s" data-wow-delay=".2s">
				<h1><a href="index.html">JESSHILL BLOG</a></h1>
			</div>
		</div>
	</div>

	<div class="services w3l wow fadeInDown"  data-wow-duration=".8s" data-wow-delay=".2s">
		<div class="container">
			<div class="grid_3 grid_5">
				<div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
					<ul id="myTab" class="nav nav-tabs" role="tablist">
						<li role="presentation" class=""><a href="" id="expeditions-tab" role="tab" data-toggle="tab" aria-controls="expeditions" aria-expanded="true">PEOPLE</a></li>
						<li role="presentation" class=""><a href="" role="tab" id="safari-tab" data-toggle="tab" aria-controls="safari">IDEAS</a></li>
						<li role="presentation" class=""><a href="" role="tab" id="trekking-tab" data-toggle="tab" aria-controls="trekking">STRATEGY</a></li>
					</ul>
					<div id="myTabContent" class="tab-content">
						<div role="tabpanel" class="tab-pane fade" id="expeditions" aria-labelledby="expeditions-tab">
							
							<div class="col-md-4 col-sm-5 tab-image">
								<img src="images/people3.jpg" style="height: 234px;" class="img-responsive" alt="Wanderer">
							</div>
							<div class="col-md-4 col-sm-5 tab-image">
								<img src="images/people2.jpg" class="img-responsive" alt="Wanderer">
							</div>
							<div class="col-md-4 col-sm-5 tab-image">
								<img src="images/one_1.jpg" style="height: 234px;" class="img-responsive" alt="Wanderer">
							</div>
							<div class="clearfix"></div>
						</div>					
						
						<div role="tabpanel" class="tab-pane fade" id="safari" aria-labelledby="safari-tab">
							<div class="col-md-4 col-sm-5 tab-image">
								<img src="images/advisory.png" style="height: 234px;" class="img-responsive" alt="Wanderer">
							</div>
							<div class="col-md-4 col-sm-5 tab-image">
								<img src="images/idea1.jpg" class="img-responsive" alt="Wanderer">
							</div>
							<div class="col-md-4 col-sm-5 tab-image">
								<img src="images/idea3.jpg" style="height: 234px;" class="img-responsive" alt="Wanderer">
							</div>
							<div class="clearfix"></div>
						</div>

						<div role="tabpanel" class="tab-pane fade active in" id="trekking" aria-labelledby="trekking-tab">

							<div class="col-md-4 col-sm-5 tab-image">
								<img src="images/people2.jpg" class="img-responsive" alt="PEOPLE">
							</div>
							<div class="col-md-4 col-sm-5 tab-image">
								<img src="images/strategy2.jpg" class="img-responsive" alt="IDEAS">
							</div>
							<div class="col-md-4 col-sm-5 tab-image">
								<img src="images/strategy3.jpg" class="img-responsive" alt="STRATEGY">
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<!-- technology-left -->
	<div class="technology">
		<div class="container">
			<div class="col-md-9 technology-left">
				<div class="tech-no">
					<!-- technology-top -->
					<?php
					    while ($row=mysqli_fetch_array($query_result,MYSQLI_ASSOC)) {
					    $id = $row['id'];
					    $title = $row['post_title'];
					    $body = $row['post_body'];
					    $image = $row['post_image'];
					    $by = $row['post_by'];
					    $date = $row['signup_date'];
					?>
					<div class="tc-ch wow fadeInDown"  data-wow-duration=".8s" data-wow-delay=".2s">			
						<div class="tch-img">
							<a href="more.php?id=<?php echo "$id"; ?>">
								<div class="tch_img_div" style="background: url(../admin/uploads/<?php echo "$image"; ?>) no-repeat 0px 0px; background-size:cover; -webkit-background-size: cover; -o-background-size: cover; -ms-background-size: cover; -moz-background-size: cover; min-height: 210px;">
								</div>
							</a>
						</div>
						
						<h3>
							<a href="more.php?id=<?php echo "$id"; ?>">
								<?php echo "$title"; ?>
							</a>
						</h3>
						<h6>BY 
							<a href="more.php?id=<?php echo "$id"; ?>">
								<?php echo "$by"; ?> 
							</a>
							<?php echo "$date"; ?>.
						</h6>
						<p>
							<?php echo "$body"; ?>
						</p>
						
						<div class="bht1">
							<a href="more.php?id=<?php echo "$id"; ?>">Continue Reading</a>
						</div>
						<div class="soci">
							<ul>
								<li class="hvr-rectangle-out"><a class="fb" href="#"></a></li>
								<li class="hvr-rectangle-out"><a class="twit" href="#"></a></li>
								<!-- <li class="hvr-rectangle-out"><a class="goog" href="#"></a></li>
								<li class="hvr-rectangle-out"><a class="pin" href="#"></a></li>
								<li class="hvr-rectangle-out"><a class="drib" href="#"></a></li> -->
							</ul>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
					<?php }?>
				</div>
				
			</div>
			<!-- technology-right -->
			<div class="col-md-3 technology-right">
				<div class="blo-top1">				
					<div class="tech-btm">
						<h4>Popular Posts </h4>
						<?php
						    while ($row=mysqli_fetch_array($recent_query_result,MYSQLI_ASSOC)) {
						    $id = $row['id'];
						    $title = $row['post_title'];
						    $body = $row['post_body'];
						    $image = $row['post_image'];
						    $by = $row['post_by'];
						    $date = $row['signup_date'];
						?>
						<div class="blog-grids wow fadeInDown"  data-wow-duration=".8s" data-wow-delay=".2s">
							<div class="blog-grid-left">
								<a href="more.php?id=<?php echo "$id"; ?>">
									<div class="tch_img_div" style="background: url(../admin/uploads/<?php echo "$image"; ?>) no-repeat 0px 0px; background-size:cover; -webkit-background-size: cover; -o-background-size: cover; -ms-background-size: cover; -moz-background-size: cover; min-height: 100px;">
									</div>
								</a>
							</div>
							<div class="blog-grid-right">
								<h5>
									<a href="more.php?id=<?php echo "$id"; ?>" style="color: #77C53E;">
										<?php echo "$title"; ?>
									</a> 
								</h5>
								<p style="white-space: nowrap; overflow: hidden;text-overflow: ellipsis; ">
									<?php echo "$body"; ?>
								</p>
							</div>
							<div class="clearfix"> </div>
						</div>
						<?php }?>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<!-- technology-right -->
		</div>
	</div>

	<div class="copyright wow fadeInDown"  data-wow-duration=".8s" data-wow-delay=".2s">
		<div class="container">
			<p>© 2017 Jesshill Consulting. All rights reserved</p>
		</div>
	</div>

</body>
</html>