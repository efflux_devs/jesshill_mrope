<?php  
	$id = $_GET['id'];

	require_once('../admin/connect.php');
	require_once('../admin/process_query.php');
?>

<!DOCTYPE HTML>
<html>
	<head>
		<title>Jesshill | Blog</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="Style Blog Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
		Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
		<script type="applijewelleryion/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
		<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
		<!-- Custom Theme files -->
		<link href='//fonts.googleapis.com/css?family=Raleway:400,600,700' rel='stylesheet' type='text/css'>
		<link href="css/style.css" rel='stylesheet' type='text/css' />
		<link rel="stylesheet" type="text/css" href="../content/css/fonts/font.css">
		<script src="js/jquery-1.11.1.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/comment.js"></script>
		<style>
			#form-messages{
		        color: green;
		        font-family: lato;
		        text-transform: capitalize;
		        font-weight: bold;
		        font-size: 15px;
		        text-align: center;
	        }
		</style>
	</head>
	<body>
		<div class="header" id="ban">
			<div class="container">
				<div class="header_right">
					<nav class="navbar navbar-default">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>

						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
							<nav class="link-effect-7" id="link-effect-7">
								<ul class="nav navbar-nav">
									<li><a href="index.php">Home</a></li>
									<li><a href="../index.php">visit main website</a></li>
								</ul>
							</nav>
						</div>
						<!-- /.navbar-collapse -->
					</nav>
				</div>
				<div class="clearfix"> </div>	
			</div>
		</div>
		<!--start-main-->
		<div class="header-bottom">
			<div class="container">
				<div class="logo">
					<h1><a href="index.php">JESSHILL BLOG</a></h1>
					<!-- <p><label class="of"></label>LET'S MAKE A PERFECT STYLE<label class="on"></label></p> -->
				</div>
			</div>
		</div>
		<!-- banner -->

		<div class="banner-1">

		</div>

		<!-- technology-left -->
		<div class="technology">
			<div class="container">
				<div class="col-md-9 technology-left">
					<div class="agileinfo">
				  		<!-- <h2 class="w3">SINGLE PAGE</h2> -->
				  		<?php 
				  			$row_id=mysqli_fetch_assoc($id_result);
				  				$id = $row_id['id'];
						    	$title = $row_id['post_title'];
							    $body = $row_id['post_body'];
							    $image = $row_id['post_image'];
							    $by = $row_id['post_by'];
							    $date = $row_id['signup_date'];
				  		?>
						<div class="single">
		   					<div class="tch_img_div" style="background: url(../admin/uploads/<?php echo "$image"; ?>) no-repeat 0px 0px; background-size:cover; -webkit-background-size: cover; -o-background-size: cover; -ms-background-size: cover; -moz-background-size: cover; min-height: 350px;">
							</div>
						    <div class="b-bottom"> 
						      	<h5 class="top">
						      	<?php echo "$title"; ?>
						      	</h5>
							    <p class="sub">
							   		<?php echo "$body"; ?>
						   		</p>
						      	<p><?php echo "$date"; ?> 
						      		<?php 
						      			while ($comment_count=mysqli_fetch_assoc($comment_count_result)) {  
										$count = $comment_count['COUNT(*)'];  
						      		?>
						      		<a class="span_link" href="#">
						      			<span class="glyphicon glyphicon-comment"></span>
						      			<?php echo "$count"; ?>
					      			</a>
					      			<!-- <a class="span_link" href="#">
					      				<span class="glyphicon glyphicon-eye-open"></span>56 
				      				</a> -->
					      		</p>
							</div>
						</div>
						
						<div class="response">
							<h4>Responses</h4>
							<?php 
					  			while ($row_comment=mysqli_fetch_array($comment_query_result)) {
								    $commentname = $row_comment['name'];
								    $commentbody = $row_comment['comment'];
								    $commentdate = $row_comment['signup_date'];
					  		?>
							<div class="media response-info">
								<div class="media-left response-text-left">
									<h1 style="font-size: 20px; text-transform: uppercase; line-height: 30px; font-weight: 600;">
										<?php echo "$commentname"; ?>
									</h1>
									<!-- <a href="#">
										<img src="images/sin1.jpg" class="img-responsive" alt="">
									</a> -->
								</div>

								<div class="media-body response-text-right">
									<p>
										<?php echo "$commentbody"; ?>
									</p>
									<ul>
										<li>
											<?php echo "$commentdate"; ?>
										</li>
									</ul>
								</div>
								
								<div class="clearfix"> </div>
							</div>
							<?php }?>
						</div>

						<div class="coment-form">
							<h4>Leave your comment</h4>
							<form action="../admin/process_comment.php" method="POST" id="commentForm">
								<input type="hidden" name="id" value="<?php echo "$id"; ?>">

								<input type="text" value="Name " name="name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name';}" required="">

								<input type="email" value="Email" name="email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">

								<!-- <input type="text" value="Website" name="websie" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Website';}" required=""> -->

								<textarea onfocus="this.value = '';" name="comment" onblur="if (this.value == '') {this.value = 'Your Comment...';}" required="">Your Comment...</textarea>

								<input type="submit" value="Submit Comment" id="submit">
							</form>
							<div id="form-messages"></div>
						</div>
						<?php } ?>	
						<div class="clearfix"></div>
					</div>
				</div>

				<!-- technology-right -->
					<div class="col-md-3 technology-right">				
						<div class="blo-top1">
							<div class="tech-btm">
								<h4>Popular Posts </h4>
								<?php
								    while ($row=mysqli_fetch_array($recent_query_result,MYSQLI_ASSOC)) {
								    $id = $row['id'];
								    $title = $row['post_title'];
								    $body = $row['post_body'];
								    $image = $row['post_image'];
								    $by = $row['post_by'];
								    $date = $row['signup_date'];
								?>
								<div class="blog-grids">
									<div class="blog-grid-left">
										<a href="more.php?id=<?php echo "$id"; ?>">
											<div class="tch_img_div" style="background: url(../admin/uploads/<?php echo "$image"; ?>) no-repeat 0px 0px; background-size:cover; -webkit-background-size: cover; -o-background-size: cover; -ms-background-size: cover; -moz-background-size: cover; min-height: 100px;">
											</div>
										</a>
									</div>
									<div class="blog-grid-right">
										<h5>
											<a href="more.php?id=<?php echo "$id"; ?>" style="color: #77C53E;">
												<?php echo "$title"; ?>
											</a> 
										</h5>
										<p style="white-space: nowrap; overflow: hidden;text-overflow: ellipsis; ">
											<?php echo "$body"; ?>
										</p>
									</div>
									<div class="clearfix"> </div>
								</div>
								<?php }?>
							</div>	
						</div>
					</div>
					<div class="clearfix"></div>
				<!-- technology-right -->
			</div>
		</div>
		
		<!-- <div class="copyright">
			<div class="container">
				<p>© 2016 Style Blog. All rights reserved | Powered by <a href="http://greymatteragency.com/" target="_blank">Greymmatter Agency</a></p>
			</div>
		</div> -->
	</body>
</html>