function onSuccessSubmission(data, status)
       {
           data = $.trim(data);
            if (data==="success") {
                $("#form-messages").html("Form submitted successfully!");
                } else {
                $("#form-messages").html(data);    
            }
       }
           
 
function onErrorSubmission(data, status)
       {
          alert(status);
       }        
 
       $(document).ready(function() {
          $("#submit").click(function(){
               var formData = new FormData($("#blogForm")[0]);
               $.ajax({
                type: "POST",
                url: "process_post.php",
                cache: false,
                crossDomain: true,
                data: formData,
                async: true,
                contentType: false,
                processData: false,
                beforeSend: function(){ $("#form-messages").html("Loading... Please wait");  },
                success: onSuccessSubmission,
                error: onErrorSubmission
               });
               return false;
           });
       });