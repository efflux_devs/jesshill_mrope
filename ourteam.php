<!DOCTYPE html>
<html lang="en">
<head>
	<title>Ourteam| Jesshill </title>
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Craft web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
	function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!--// Meta tag Keywords -->
	<!-- css files -->
	<link rel="stylesheet" href="css/bootstrap.css"> <!-- Bootstrap-Core-CSS -->
	<link rel="stylesheet" href="css/ourteamstyle.css" type="text/css" media="all" /> <!-- Style-CSS --> 
	<link rel="stylesheet" href="css/font-awesome.css"> <!-- Font-Awesome-Icons-CSS -->
	<link rel='stylesheet' type='text/css' href='css/jquery.easy-gallery.css' /> <!-- For-gallery-CSS -->
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/test.css">
	<link rel="stylesheet" type="text/css" href="css/fonts/font.css">
	<!-- //css files -->

	<!-- js -->
		<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
	<!-- //js -->

	<style>
		.mySlides {display:none;height: 600px;}
		.dropdown-menu a:hover {background-color: #f1f1f1;}
		.dropdown:hover .dropdown-menu {
		    display: block;
		}
		.dropdown-menu a {
			text-transform: uppercase;
		}
	</style>
</head>
<body>
	<?php
        include ("header_bottom.php");
    ?>

	<?php
        include ("header.php");
    ?> 
	
	<section class="banner-w3ls2">
		<div class="container">
			<h1 class="text-center agileits-w3layouts agile w3-agile" style="color: #B0CE2D; font-weight: 600; margin-top: 150px !important;">
				<!-- Giving The Best Solutions -->
			</h1>
		</div>
	</section>
<!-- //main -->
	<!-- Services --> 
		<div class="popular-w3" id="services">
			<div class="thecontent" style="background-color: #606060;-moz-box-shadow: 0 0 5px #888;-webkit-box-shadow: 0 0 5px#888;box-shadow: 0 0 5px #888; height: auto; padding: 50px 40px;">
				<p style="font-size: 15px; color: #fff; font-family: opensans; line-height: 30px; text-align: justify;">
					Our people are our greatest asset. They are unique – very energetic about supporting and challenging our clients, both at the same time.<br><br>

					Our team draw on its diverse skill set, years of top management consulting and senior line management experience to offer our clients the highest quality service. With over a century of combined experience in areas of leadership, sales, project management, customer service management, business planning & finance, motivation and personal development, we are passionate about making meaningful and measurable impact in all we do. <br><br>

					Our team comprise of smart, driven people who care a lot more about getting it done, and the relationships we build. We know that every team is as strong as its weakest link. Therefore we all strive for continuous improvement and training. We do not consider ourselves to be perfectionists. However, we drill and stretch ourselves towards excellence.  We are an extension of  our clients' teams. Our collaborative style emphasizes teamwork, integrity, creativity, knowledge and trust. Our approach to work is non conventional yet effective. This stands us out.<br>
					We also have fun,….lots of it, while working with our clients towards the growth of their organisations
				</p>
			</div>
		</div>
	<!-- //Services --> 

	<!-- team -->
	<script src="js/jquery.vide.min.js"></script>	
	<!-- <div data-vide-bg="video/Conference">
		<div class="center-container">
			<div class="team-w3l" id="team">
			   <div class="container">
			     <h3 class="title-w3" style="text-align: center; padding-top: 30px; font-size: 50px; color: #B0CE2D;">Our Team</h3>
			      <div class="team-grids">
							<div class="col-sm-4 team-grid">
								<div class="ih-item circle effect10 bottom_to_top">
									<div class="img"><img src="images/placeholder.gif" alt="img" /></div>
									<div class="info">
										<h3>Yaqub Gowon</h3>
										<div class="icons">
											<ul>
												<li><a href="#"><i class="fa fa-facebook"></i></a></li>
												<li class="team-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
												<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-4 team-grid">
								<div class="ih-item circle effect10 bottom_to_top">
									<div class="img"><img src="images/placeholder.gif" alt="img" /></div>
									<div class="info">
										<h3>Yaqub Gowon</h3>
										<div class="icons">
											<ul>
												<li><a href="#"><i class="fa fa-facebook"></i></a></li>
												<li class="team-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
												<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-4 team-grid">
								<div class="ih-item circle effect10 bottom_to_top">
									<div class="img"><img src="images/placeholder.gif" alt="img" /></div>
									<div class="info">
										<h3>Yaqub Gowonr</h3>
										<div class="icons">
											<ul>
												<li><a href="#"><i class="fa fa-facebook"></i></a></li>
												<li class="team-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
												<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-4 team-grid">
								<div class="ih-item circle effect10 bottom_to_top">
									<div class="img"><img src="images/placeholder.gif" alt="img" /></div>
									<div class="info">
										<h3>Yaqub Gowon</h3>
										<div class="icons">
											<ul>
												<li><a href="#"><i class="fa fa-facebook"></i></a></li>
												<li class="team-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
												<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-4 team-grid">
								<div class="ih-item circle effect10 bottom_to_top">
									<div class="img"><img src="images/placeholder.gif" alt="img" /></div>
									<div class="info">
										<h3>Yaqub Gowon</h3>
										<div class="icons">
											<ul>
												<li><a href="#"><i class="fa fa-facebook"></i></a></li>
												<li class="team-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
												<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-4 team-grid">
								<div class="ih-item circle effect10 bottom_to_top">
									<div class="img"><img src="images/placeholder.gif" alt="img" /></div>
									<div class="info">
										<h3>Yaqub Gowon</h3>
										<div class="icons">
											<ul>
												<li><a href="#"><i class="fa fa-facebook"></i></a></li>
												<li class="team-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
												<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="clearfix"> </div>
					</div>

			  </div>	
			</div>	
		</div>
	</div> -->
	
	<section class="banner-w3ls3">
		<div class="container">
		</div>
	</section>

	<?php
        include ("footer.php");
    ?> 


<!-- js-scripts -->					
		
			<script type="text/javascript" src="js/bootstrap.js"></script> <!-- Necessary-JavaScript-File-For-Bootstrap --> 
			
		<!-- Baneer-js -->
		<script src="js/responsiveslides.min.js"></script>
		<script>
				$(function () {
					$("#slider").responsiveSlides({
						auto: true,
						pager:false,
						nav: true,
						speed: 1000,
						namespace: "callbacks",
						before: function () {
							$('.events').append("<li>before event fired.</li>");
						},
						after: function () {
							$('.events').append("<li>after event fired.</li>");
						}
					});
				});
			</script>
		<!-- //Baneer-js -->
		
		<!-- For-Gallery-js -->
			<!-- script for portfolio -->
			<script type='text/javascript' src='js/jquery.easy-gallery.js' ></script>
			<script type='text/javascript'>
			  //init Gallery
			  $('.portfolio').easyGallery();
			</script>
			<script src="js/easyResponsiveTabs.js" type="text/javascript"></script>
			<script type="text/javascript">
				$(document).ready(function () {
					$('#horizontalTab').easyResponsiveTabs({
						type: 'default', //Types: default, vertical, accordion           
						width: 'auto', //auto or any width like 600px
						fit: true   // 100% fit in a container
					});
				});		
			</script>
			<!-- //script for portfolio -->
		<!-- //For-Gallery-js -->

		<!-- start-smoth-scrolling -->
				<script type="text/javascript" src="js/move-top.js"></script>
				<script type="text/javascript" src="js/easing.js"></script>
				<script type="text/javascript">
					jQuery(document).ready(function($) {
						$(".scroll").click(function(event){		
							event.preventDefault();
							$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
						});
					});
				</script>
		<!-- start-smoth-scrolling -->

<!-- //js-scripts -->
</body>
</html>