<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Jesshill | About Us</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" />
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
		<!-- Custom Theme files -->
		<link href="css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
		<link href="css/whowearestyle.css" type="text/css" rel="stylesheet" media="all">
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<link rel="stylesheet" type="text/css" href="css/test.css">
		<link href="css/fonts/font.css" type="text/css" rel="stylesheet" media="all">
		<link href="css/font-awesome.css" rel="stylesheet">   
		<link rel="stylesheet" type="text/css" href="css/fonts/font.css">
		<!-- font-awesome icons --> 
		<!-- //Custom Theme files -->  
		<!-- js --> 
		<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
	</head>
	<style>
		.mySlides {display:none;height: 600px;}
		.dropdown-menu a:hover {background-color: #f1f1f1;}
		.dropdown:hover .dropdown-menu {
		    display: block;
		}
		.dropdown-menu a {
			text-transform: uppercase;
		}
		.side_contact {
			width: 90%;
			height: auto;
			background-color: #B0CE2D;
			padding: 20px 40px;
		}

		.button {
			text-align: center;
			width: 80%;
		    margin: auto;
		    display: block;
		    text-align: center;
		    font-weight: 600;
		    font-family: montserratReg;
		    font-size: 20px;
		}

		button.accordion {
		    background-color: #eee;
		    color: #444;
		    cursor: pointer;
		    padding: 18px;
		    font-family: montserratReg;
		    width: 100%;
		    border: none;
		    text-align: left;
		    outline: none;
		    font-size: 15px;
		    text-transform: uppercase;
		    transition: 0.4s;
		}

		button.accordion.active, button.accordion:hover {
		    background-color: #ddd;
		}

		button.accordion:after {
		    content: '\002B';
		    color: #777;
		    font-weight: bold;
		    float: right;
		    margin-left: 5px;
		}

		button.accordion.active:after {
		    content: "\2212";
		}

		div.panel {
		    padding: 0 18px;
		    background-color: #F2F2F2;
		    max-height: 0;
		    line-height: 30px;
		    overflow: hidden;
		    transition: max-height 0.2s ease-out;
		}

		.float_left {
			float: left;
			width: 10%;
		}
		.float_right {
			float: right;
			padding-left: 20px;
			width: 90%;
		}
		.fa_square {
			font-size: 40px;
		    color: #fff;
		    width: 90%;
		    background: #251021;;
		    height: 50%;
		    text-align: center;
		    border-radius: 30%;
		    margin-right: 20px;
		}
		.float_right_h1 {
			font-size: 18px;
			text-transform: capitalize;
			font-family: montserratReg;
			margin-bottom: 15px;
			font-weight: 600;
		}
	</style>
<body> 
	<?php
        include ("header_bottom.php");
    ?>

	<?php
        include ("header.php");
    ?>  

	<!-- banner -->
	<!-- <div class="banner">
		
	</div> -->

	<div class="">
  		<div id="myCarousel" class="carousel slide" data-ride="carousel">
    		<div class="carousel-inner">
		      <div class="item active">
		        <img src="images/w.jpg" alt="Los Angeles" style="width:100%;height: 600px;">
		        <div class="carousel-caption">
		          <h1 class="h1" style="color: #fff; text-align: center;font-weight: 600; margin-bottom: 250px!important; font-size: 100px; font-family: montserratReg;">INNOVATE</h1>
		        </div>
		      </div>

		      <div class="item">
		        <img src="images/we.jpg" alt="Chicago" style="width:100%;height: 600px;">
		        <div class="carousel-caption">
		          <h1 class="h1" style="color: #fff; font-weight: 600;margin-bottom: 150px!important; font-size: 100px; font-family: montserratReg;">BOOST PRODUCTIVITY</h1>
		        </div>
		      </div>
    
		      <!-- <div class="item">
		        <img src="images/wee.jpg" alt="New York" style="width:100%;height: 600px;">
		        <div class="carousel-caption">
		          <h1 class="h1" style="color: #fff; font-weight: bold; margin-bottom: 40px!important; font-size: 100px; font-family: montserratReg;">GET VISIBILITY & ACHIEVE GREATER SUCCESS</h1>
		        </div>
		      </div> -->
    		</div>
  		</div>
	</div>

	<div class="">
		<div class="div" style="background-color: #F2F2F2; padding: 30px;">
			<div class="container">
				<div class="col-md-12 w3layouts_register_right">
					<h1 style="font-size:36px; font-weight:600; color:#251021; text-transform: uppercase; text-align: center; font-family: montserratReg;">
						WHO WE ARE
					</h1>
					<p style="text-align: center; width: 3%; margin: auto; height: 7px; background-color: #77C53E; margin-bottom: 2em;"></p>

					<div class="container">
						<p style="color: #777777; padding: 20px 20px; font-size: 15px; line-height: 30px; text-align: justify;">
							Jesshill Consulting is a peak performance, strategic training and ideas generation company with a unique approach to partnering with our clients and finding solutions to their business needs.<br><br>
							We are passionate about creating and managing change in organizations by formulating and implementing customer focused strategies. We help our clients with their organization's most critical issues and opportunities. Registered with the Nigerian Corporate Affairs commission in 2010, we are fanatical about improving the capabilities of our clients. <br> 
						</p>

						<p style="font-size: 15px; padding: 20px 20px 20px 10px; background-color: #5D5D5D; margin-left: 20px; border-left: 10px solid #77C53E; color: #fff; text-align: justify;">
							<em>
								At Jesshill Consulting, our assignment runs deeper than the services we offer. It is our customers’ experience that is important to us. Therefore, we are committed to supporting their sustainable growth. This commitment is the vital source from which our energy, style and values manifests themselves.
							</em>
						</p>

						<p style="font-size: 15px; padding: 20px 20px; line-height: 30px; color: #777777;">
							We collaborate and work with companies in diverse sectors in the areas of <b>TRAINING</b>, <b>ADVISORY</b> <b>SERVICES</b> and 
							<b>
								TALENT HUNT
							</b>
						</p>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>

	<div class="container" style="margin-top: 30px;">
		<div class="container" style="margin-bottom: 30px;">
			<div class="row">
				<div class="col-md-6">
					<div class="float_left">
						<img src="images/teaching.png">
					</div>
					<div class="float_right">
						<p style="font-size: 15px; font-family: opensans; color: #777777; line-height: 30px; text-align: justify;">
							Our <b>TRAININGS</b> are easily transferable to the workplace. We aim to strike a balance between presenting essential background information and facilitating participative exercises that are both challenging and motivating thereby enabling experiential learning to take place. 
						</p>
					</div>
				</div>
				<div class="col-md-6">
					<div class="float_left">
						<img src="images/recruitmenticon.png" style="height: 60px;">
					</div>
					<div class="float_right">
						<p style="font-size: 15px; font-family: opensans; color: #777777; line-height: 30px; text-align: justify;">
							Through our <b>RECRUITMENT</b> process, we find and place professionals and senior management executives for companies across all industries. We understand the frustrations and pitfalls of searching for and recruiting high-calibre individuals by organisations and bring to bear our expertise at helping organisations scale this hurdle.
						</p>
					</div>
				</div>
			</div>

			<div class="row" style="margin-top: 30px;">
				<div class="col-md-6">
					<div class="float_left">
						<img src="images/advisoryicon.png">
					</div>
					<div class="float_right">
						<p style="font-size: 15px; font-family: opensans; color: #777777; line-height: 30px; text-align: justify;">
							Our <b>ADVISORY</b> service platform provide startups, existing SMEs and established businesses the support, objectivity and expertise needed by them to succeed within the context of an ever-changing business landscape.
						</p>
					</div>
				</div>
			</div>
		</div>
		<p style="font-size: 15px; padding: 20px 20px 20px 10px; background-color: #5D5D5D; margin-left: 20px; border-left: 10px solid #77C53E; color: #fff; text-align: center;">
			<em>
				Working with organizations in their quest to seize competitive advantage and win is Jesshill Consulting’s raison d'être
			</em>
		</p>

		<p style="font-size: 15px; padding: 20px 20px; line-height: 30px; color: #777777;">
			We offer a broad spectrum of consulting services across sectors of the economy (Courier, e-Commerce, Financial, Pharmaceuticals, Airline, Educational, Religious,..etc ) to give business owners and managers the insight they need to thrive.
		</p>
	</div>
	
	<!--footer-->
		<?php 
			include ("footer.php");
		?>
	<!--//footer-->	

	<!-- banner Slider starts Here -->
	<script src="js/responsiveslides.min.js"></script>
	<script>
		// You can also use "$(window).load(function() {"
		$(function () {
		  // Slideshow 3
		  $("#slider3").responsiveSlides({
			auto:true,
			pager:false,
			nav: true,
			speed: 500,
			namespace: "callbacks",
			before: function () {
			  $('.events').append("<li>before event fired.</li>");
			},
			after: function () {
			  $('.events').append("<li>after event fired.</li>");
			}
		  });
	
		});
	</script>
	<script src="js/SmoothScroll.min.js"></script>
	<!-- smooth-scrolling-of-move-up -->
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			/*
			var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
			};
			*/
			
			$().UItoTop({ easingType: 'easeOutQuart' });
			
		});
	</script>
	<!-- //smooth-scrolling-of-move-up -->  

	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/bootstrap.js"></script>
	<script>
		var myIndex = 0;
		carousel();

		function carousel() {
		    var i;
		    var x = document.getElementsByClassName("mySlides");
		    for (i = 0; i < x.length; i++) {
		       x[i].style.display = "none";  
		    }
		    myIndex++;
		    if (myIndex > x.length) {myIndex = 1}    
		    x[myIndex-1].style.display = "block";  
		    setTimeout(carousel, 3000); // Change image every 2 seconds
		}
	</script>

	<script>
		var acc = document.getElementsByClassName("accordion");
		var i;

		for (i = 0; i < acc.length; i++) {
		    acc[i].onclick = function(){
		        this.classList.toggle("active");
		        var panel = this.nextElementSibling;
		        if (panel.style.display === "block") {
		            panel.style.display = "none";
		        } else {
		            panel.style.display = "block";
		        }
		    }
		}
	</script>

	<script>
		var acc = document.getElementsByClassName("accordion");
		var i;

		for (i = 0; i < acc.length; i++) {
		  acc[i].onclick = function() {
		    this.classList.toggle("active");
		    var panel = this.nextElementSibling;
		    if (panel.style.maxHeight){
		      panel.style.maxHeight = null;
		    } else {
		      panel.style.maxHeight = panel.scrollHeight + "px";
		    } 
		  }
		}
	</script>
</body>
</html>