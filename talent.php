<!DOCTYPE html>
<html lang="en">
	<head>
		<title></title>
		<!-- custom-theme -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="Driving School Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
		Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
				function hideURLbar(){ window.scrollTo(0,1); } </script>
		<!-- //custom-theme -->
		<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/whatwedostyle.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/stylee.css" rel="stylesheet" type="text/css" media="all" />
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<link rel="stylesheet" type="text/css" href="css/fonts/font.css">
		<link rel="stylesheet" href="css/test.css">
		<!-- js -->
		<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
		<!-- //js -->
  		<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
		<link href="css/font-awesome.css" rel="stylesheet"> 
		<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" property="" />
		<!-- //font-awesome-icons -->
	</head>
	<style>
		.mySlides {display:none;height: 600px;}
		.dropdown-menu a:hover {background-color: #f1f1f1;}
		.dropdown:hover .dropdown-menu {
		    display: block;
		}
		.dropdown-menu a {
			text-transform: uppercase;
		}

		.side_contact {
			width: 90%;
			height: auto;
			background-color: #77C33E;
			padding: 20px 40px;
		}

		.button {
			text-align: center;
			width: 80%;
		    margin: auto;
		    display: block;
		    text-align: center;
		    color: #777777;
		    cursor: pointer;
		    font-weight: 600;
		    font-family: montserratReg;
		    font-size: 20px;
		  }

		.button:hover {
			color: #251021;
		}

		button.accordion {
		    background-color: #eee;
		    color: #251021;
		    cursor: pointer;
		    padding: 18px;
		    font-family: montserratReg;
		    width: 100%;
		    border: none;
		    text-align: left;
		    outline: none;
		    font-size: 15px;
		    text-transform: uppercase;
		    transition: 0.4s;
		}

		button.accordion.active, button.accordion:hover {
		    background-color: #ddd;
		}

		button.accordion:after {
		    content: '\002B';
		    color: #777;
		    font-weight: bold;
		    float: right;
		    margin-left: 5px;
		}

		button.accordion.active:after {
		    content: "\2212";
		}

		div.panel {
		    padding: 0 18px;
		    background-color: #F2F2F2;
		    max-height: 0;
		    line-height: 30px;
		    overflow: hidden;
		    transition: max-height 0.2s ease-out;
		}
	</style>
	<body>
		<?php
	        include ("header_bottom.php");
	    ?>

		<?php
	        include ("header.php");
	    ?>

		<div class="w3-container">
	 
		</div>

		<div class="">
	  		<div id="myCarousel" class="carousel slide" data-ride="carousel">
	    		<div class="carousel-inner">
			      <div class="item active">
			        <img src="images/new_talent1.png" alt="Los Angeles" style="width:100%;height: 600px;">
			        <div class="carousel-caption">
			          <h1 style="color: #fff; text-align: center; font-weight: 600; margin-bottom: 200px!important; font-size: 100px; font-family: montserratReg;">TALENT HUNT</h1>
			        </div>
			      </div>

			      <div class="item">
			        <img src="images/new_talent.jpg" alt="Chicago" style="width:100%;height: 600px;">
			        <div class="carousel-caption">
			          <h1 style="color: #fff; font-weight: 600; margin-bottom: 200px!important; font-size: 100px; font-family: montserratReg;"> RECRUITMENT</h1>
			        </div>
			      </div>
	    			
			      <div class="item">
			        <img src="images/new_talent2.jpg" alt="New York" style="width:100%;height: 600px;">
			        <div class="carousel-caption">
			          <h1 style="color: #fff; font-weight: 600; margin-bottom: 200px!important; font-size: 100px; font-family: montserratReg;">SELECTION</h1>
			        </div>
			      </div>
	    		</div>
	  		</div>
	  	</div>
		
		<div class="">
			<div class="div" style="background-color: #fff;">
				<div class="container">
					<div class="col-md-12 w3layouts_register_right">
						<p style="color: #777777; font-size: 15px; margin-bottom: 20px; padding-top: 30px; line-height: 30px; text-align: justify;">
							We are committed to collaborating with our clients at finding great talents. We employ high level expertise to the task of finding the best candidate for a vacancy. Our hiring decisions are secure and infallible. Our process ensures that candidates are tested on multiple levels. We pride in our ability to source talented performers quickly who in turn drive business growth. <br><br>
							<b>Our recruitment process include:</b>
						</p>
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="container">
					<div class="row">
						<div class="col-md-8">
							<div style="padding: 20px;">
								<button class="accordion">Resourcing Strategy Design and Delivery</button>
								<div class="panel">
								  	<p style="font-size: 15px; color: #777777; text-align: justify;">
								  		At Jesshill Consulting, we believe that ‘The Resourcing Cycle’ (including role competency profiling through to the actual selection and induction delivery) must shape and reflect the workforce strategy for adding value to the organization.<br>
 
										Typical Client challenges include but not limited to aligning corporate strategy with resourcing strategy. Our value solutions are enabled to take care of the following:<br>
 
										-Outlining and designing a bespoke resourcing strategy that adds value to organizations.<br>
										-Providing the professional know-how to facilitate the implementation of strategy.
								  	</p>
								</div>

								<button class="accordion">Pre-employment Screening Services</button>
								<div class="panel">
								  	<p style="font-size: 15px; color: #777777; text-align: justify;">
								  		Human resources are an organization’s greatest asset. Therefore, getting it right from the start is essential to the success of any establishment. Selecting the best fit for a role poses many challenges and organisations are recognising the cost of getting recruitment decisions wrong. <br>
										At Jesshill Consulting, we offer a range of recruitment and selection services to give our clients the confidence of knowing that they are selecting the right people. These services include: <br>

										-Aptitude test administration <br>
										-Psychometric testing <br>
										-Competency based interviews <br><br>
										Our approach include but is not limited to the following:<br>
										-Presenting shortlist of the top 1% candidates for the specific job profile<br>
										-Conducting preliminary ‘in person’ interviews<br>
										-Assisting in the preparation of selection documentation<br>
										-Testing candidates on qualitative and quantitative skills<br>
										-Screening candidates on soft skills such as motivation, work ethic, communication, character, etc.<br>
										-Checking candidate for cultural fitness in company’s work environment<br>
										-Perform reference checks (if required).
							  		</p>
								</div>

								<button class="accordion">Induction Design and Delivery</button>
								<div class="panel">
								  	<p style="font-size: 15px; color: #777777; text-align: justify;" >
								  		Integration into the workspace is critical and important for the organisation and new employee(s). It is imperative to have a guaranteed smooth transition into the work environment. Research has shown that tailor-made induction programmes increase staff retention.

										At Jesshill Consulting, we are committed to our clients’ induction needs through:<br>

										-Providing guidance on setting up and running induction programmes<br>
										-Designing tailor made induction programmes to ensure smooth transition for new employees<br>
										-Professional facilitation of the induction process
								  	</p>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div style="padding: 20px;">
								<div class="side_contact">
									<h2 style="margin-bottom: 15px; font-size: 20px; font-weight: 600; font-family: montserratReg; text-align: center;">How can we help you?</h2>
									<a href="contact.php" class="button">Contact Us</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<section class="banner-w3ls32">
			<div class="container">
			</div>
		</section>		
		

		<?php
	        include ("footer.php");
	    ?>

	<!-- for bootstrap working -->
		<script src="js/bootstrap.js"></script>
	<!-- //for bootstrap working -->
	<!-- here stars scrolling icon -->
		<script type="text/javascript">
			$(document).ready(function() {
				/*
					var defaults = {
					containerID: 'toTop', // fading element id
					containerHoverID: 'toTopHover', // fading element hover id
					scrollSpeed: 1200,
					easingType: 'linear' 
					};
				*/
									
				$().UItoTop({ easingType: 'easeOutQuart' });
									
				});
		</script>

		<script>
			var myIndex = 0;
			carousel();

			function carousel() {
			    var i;
			    var x = document.getElementsByClassName("mySlides");
			    for (i = 0; i < x.length; i++) {
			       x[i].style.display = "none";  
			    }
			    myIndex++;
			    if (myIndex > x.length) {myIndex = 1}    
			    x[myIndex-1].style.display = "block";  
			    setTimeout(carousel, 3000); // Change image every 2 seconds
			}
		</script>

		<script>
            var leftOffset = 0;
            var moveHeading = function () {
           
            $("#heading").offset({ left: leftOffset });
            leftOffset++;
            if (leftOffset > 1200) {
            leftOffset = 0;
            }
            };
            setInterval(moveHeading, 30);
        </script>

        <script>
			var acc = document.getElementsByClassName("accordion");
			var i;

			for (i = 0; i < acc.length; i++) {
			    acc[i].onclick = function(){
			        this.classList.toggle("active");
			        var panel = this.nextElementSibling;
			        if (panel.style.display === "block") {
			            panel.style.display = "none";
			        } else {
			            panel.style.display = "block";
			        }
			    }
			}
		</script>

		<script>
			var acc = document.getElementsByClassName("accordion");
			var i;

			for (i = 0; i < acc.length; i++) {
			  acc[i].onclick = function() {
			    this.classList.toggle("active");
			    var panel = this.nextElementSibling;
			    if (panel.style.maxHeight){
			      panel.style.maxHeight = null;
			    } else {
			      panel.style.maxHeight = panel.scrollHeight + "px";
			    } 
			  }
			}
		</script>
	<!-- //here ends scrolling icon -->
	</body>
</html>