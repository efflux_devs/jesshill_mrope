<?php
  function active($currect_page){
    $url_array =  explode('/', $_SERVER['REQUEST_URI']) ;
    $url = end($url_array);  
    if($currect_page == $url){
      echo 'active'; //class name in css 
    }else {
      echo "NotActive";
    } 
  }
?>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- <a href="#" class="navbar-brand">Brand</a> -->
        </div>
        <!-- Collection of nav links and other content for toggling -->
        <div id="navbarCollapse" class="collapse navbar-collapse" style="float:right;">
            <ul class="nav navbar-nav centered" style="font-family: montserratReg;">
              <li><a class="<?php active('index.php');?>" href="index.php">Home</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  About Us <b class="caret"></b>
                </a>
                <ul class="dropdown-menu agile_short_dropdown">
                  <li><a href="whoweare.php">Who we are</a></li>
                  <li><a href="vision.php">Vision and Mission</a></li>
                  <li><a href="ourteam.php">Our Team</a></li>
                </ul>
              </li>
              <li class="dropdown">
                <a class="<?php active('whatwedo.php');?>" href="">
                  What we do
                  <b class="caret"></b>
                </a>
                  <ul class="dropdown-menu agile_short_dropdown">
                    <li><a href="training.php">Training</a></li>
                    <li><a href="advisory.php">Advisory</a></li>
                    <li><a href="talent.php">Talent Hunt</a></li>
                  </ul>
              </li>
              <li><a class="<?php active('contact.php');?>" href="contact.php">Contact US</a></li>
              <li><a class="<?php active('index.php');?>" href="blog/index.php">Blog</a></li>
            </ul>
        </div>
    </div>
</nav>