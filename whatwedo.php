<!DOCTYPE html>
<html lang="en">
	<head>
		<title></title>
		<!-- custom-theme -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="Driving School Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
		Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
				function hideURLbar(){ window.scrollTo(0,1); } </script>
		<!-- //custom-theme -->
		<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/whatwedostyle.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/stylee.css" rel="stylesheet" type="text/css" media="all" />
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<link rel="stylesheet" type="text/css" href="css/fonts/font.css">
		<link rel="stylesheet" href="css/test.css">
		<!-- js -->
		<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
		<!-- //js -->
  		<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
		<link href="css/font-awesome.css" rel="stylesheet"> 
		<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" property="" />
		<!-- //font-awesome-icons -->
	</head>
	<style>
		.mySlides {display:none;height: 600px;}
		.dropdown-menu a:hover {background-color: #f1f1f1;}
		.dropdown:hover .dropdown-menu {
		    display: block;
		}
		.dropdown-menu a {
			text-transform: uppercase;
		}
	</style>
<body>
	<?php
        include ("header_bottom.php");
    ?>

	<?php
        include ("header.php");
    ?>

	<div class="w3-container">
 
	</div>

	<div class="">
  		<div id="myCarousel" class="carousel slide" data-ride="carousel">
    		<div class="carousel-inner">
		      <div class="item active">
		        <img src="images/eii.jpg" alt="Los Angeles" style="width:100%;height: 600px;">
		        <div class="carousel-caption">
		          <h1 style="color: #fff;text-align: center;font-weight: bold;margin-bottom: 200px !important; font-size: 70px;">GROW REVENUE</h1>
		        </div>
		      </div>

		      <div class="item">
		        <img src="images/ee.jpg" alt="Chicago" style="width:100%;height: 600px;">
		        <div class="carousel-caption">
		          <h1 style="color: #fff;font-weight: bold;margin-bottom: 200px !important; font-size: 70px;">BOOST PRODUCTIVITY</h1>
		        </div>
		      </div>
    			
		      <div class="item">
		        <img src="images/image.jpg" alt="New York" style="width:100%;height: 600px;">
		        <div class="carousel-caption">
		          <h1 style="color: #fff;font-weight: bold;margin-bottom: 200px !important; font-size: 70px;">GET VISIBILITY & ACHIEVE GREATER SUCCESS</h1>
		        </div>
		      </div>
    		</div>
  		</div>
  	</div>
	<div class="banner-bottom-icons">
		<div class="">
			<!-- <div class="w3l-heading" style="padding: 50px 0px; text-align: center; text-transform: uppercase;">
				<h2 class="w3ls_head" style="font-size: 40px !important; font-family: Museo; font-weight: 600; color: #B0CE2D;"> what we do </h2>
			</div> -->
			<div class="col-md-12 w3_banner_bottom_icons_right" style="padding: 0px 0px 20px 0px;background-color: #606060;-moz-box-shadow: 0 0 5px #888;-webkit-box-shadow: 0 0 5px#888;box-shadow: 0 0 5px #888;">
				<div class="container">
					<h1 class="text-center" style="font-family: Museo; color: #B0CE2D;padding: 20px 0px; font-weight: 600; font-size: 40px;">TRAINING</h1>
				<p style="padding: 0px;color: #fff;padding: 20px 20px; font-size: 20px;">We work with organisations to make the workplace "suicide safer". Depending on our client's need, our services entail making presentation and organizing training programs. Our training and interventions are easily transferrable to the workplace. We aim to strike a balance between presenting essential background information and facilitating participative exercises that are both challenging and motivating thereby enabling experiential learning to take place. <br> <br> Our training have become known for enabling learning through the exchange of experiences and ideas, facilitating motivating practical exercises to consolidate learning as well as presenting key ideas in straight forward and memorable ways. <br> <br> <b> We have carved a niche for ourselves in the following areas:</b>
					<ul style="padding: 0px 20px 20px 20px; color: #fff; margin-top: 0px;">
						<li class="ul_li">Customer Service Excellence/ Relationship management</li>
						<li class="ul_li">Leadership development and management</li>
						<li class="ul_li">Sales and marketing</li>
						<li class="ul_li">Strategic Management</li>
						<li class="ul_li">Project Management</li>
						<li class="ul_li">Business planning and finance</li>
						<li class="ul_li">Motivation and personal development</li>
					</ul>
				</p>
				</div>
				<div class="clearfix"></div>
			</div>
			<section class="banner-w3ls31">
				<div class="container">
				</div>
			</section>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="">
		<div class="div">
			<div class="container">
				<div class="col-md-12 w3layouts_register_right">
					<h1 class="" style="font-family: Museo; margin-top: 20px !important; color: #B0CE2D; text-align: center; padding-bottom: 10px; font-weight: 600; font-size: 40px;">ADVISORY</h1>
					<p style="color: #000;font-size: 20px;margin-bottom: 50px;">At Jesshill Consulting, we are committed to helping our clients make discoveries of actionable insights on their pressing business issues. We seek to address their most complex and interesting opportunities which are imperative to business success.</p>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<section class="banner-w3ls32">
		<div class="container">
		</div>
	</section>
	<section class="info-w3ls2" style="background-color:  #B0CE2D; padding: 0px 0px 30px 0px;">
		<div class="container">
			<h1 class="text-center" style="font-family: Museo; margin-top: 20px !important; color: #fff; text-align: center; padding-bottom: 10px; font-weight: 600; font-size: 40px;">TALENT HUNT</h1>
			<p class="text-center" style="text-align: justify; font-size: 20px;">
				At Jesshill Consulting, we are committed to collaborating with our clients at finding great talents. We employ high level expertise to the task of finding the best candidate for a vacancy. Our hiring decisions are secure and infallible. Our process ensures that candidate are tested on multiple levels using advanced candidate intelligence. We pride on our ability to source talented performers quicly who in turn drive business growth. <br> <br> <b>Our Recruitment process include:</b>
				<ul style="padding: 20px 0px 0px 0px; color: #000; margin-top: 0px;">
					<li class="ul_li">Presenting shortlist of the top 1% candidates for the specific job profile</li>
					<li class="ul_li">Conducting prelimminary 'in person' nterviews </li>
					<li class="ul_li">Assist in the preparation of selection documentation</li>
					<li class="ul_li">Test candidates on qualitative and quantitative skills</li>
					<li class="ul_li">Screen candidate on soft skills such as motivation,, work ethic, communication,character etc</li>
					<li class="ul_li">Check candidate for cultural fitness in company's work environment</li>
					<li class="ul_li">Mediate candidate's interviews in company's office</li>
					<li class="ul_li">Assist in salary negotiation and discussion</li>
					<li class="ul_li">Perform reference check (if required)</li>
				</ul>
			</p>
		</div>
	</section>		
	

	<?php
        include ("footer.php");
    ?>

<!-- for bootstrap working -->
	<script src="js/bootstrap.js"></script>
<!-- //for bootstrap working -->
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
	<script>
		var myIndex = 0;
		carousel();

		function carousel() {
		    var i;
		    var x = document.getElementsByClassName("mySlides");
		    for (i = 0; i < x.length; i++) {
		       x[i].style.display = "none";  
		    }
		    myIndex++;
		    if (myIndex > x.length) {myIndex = 1}    
		    x[myIndex-1].style.display = "block";  
		    setTimeout(carousel, 3000); // Change image every 2 seconds
		}
	</script>
	<script>
            var leftOffset = 0;
            var moveHeading = function () {
           
            $("#heading").offset({ left: leftOffset });
            leftOffset++;
            if (leftOffset > 1200) {
            leftOffset = 0;
            }
            };
            setInterval(moveHeading, 30);
        </script>
<!-- //here ends scrolling icon -->
</body>
</html>