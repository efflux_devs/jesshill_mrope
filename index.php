<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Jesshill | Home</title>
		<!-- custom-theme -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="Oil Industry Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
		Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
				function hideURLbar(){ window.scrollTo(0,1); } </script>
		<!-- //custom-theme -->
		<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
		<link rel="stylesheet" type="text/css" href="css/fonts/font.css">
		<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
		<link rel="stylesheet" type="text/css" href="css/style3.css" />
		<link rel="stylesheet" type="text/css" href="css/test.css">
		<link rel="stylesheet" type="text/css" href="css/team.css" /><!--Banner Text-->
		<!-- font-awesome-icons -->
		<link href="css/font-awesome.css" rel="stylesheet"> 
		<link href="css/animate.css" rel="stylesheet"> 
		<!-- //font-awesome-icons -->

		<style>
			.rotateIn {
			    -webkit-animation-duration: 5s;
			    animation-duration: 5s;
			}
			.fadeInRight {
			    -webkit-animation-duration: 5s;
			    animation-duration: 5s;
			}
			.slideInUp {
			    -webkit-animation-duration: 5s;
			    animation-duration: 5s;
			}
			.dropdown-menu a:hover {background-color: #f1f1f1;}
			.dropdown:hover .dropdown-menu {
			    display: block;
			}
			.dropdown-menu a {
				text-transform: uppercase;
			}
			.carousel-caption .h3 {
				animation-delay: 1s;
				font-size: 100px;
				letter-spacing: -3;
			}
			.col-md-4.the_catalog_img_parent {
			    min-height: 400px;
			    max-height: 400px;
			    overflow: hidden;
			}
			.col-md-4.the_catalog_content_parent {
			    height: auto;
			}
			.the_catalog_img_1 {
				height: 200px;
				max-height: 200px;
				background-image: url(images/new_training.jpg);
				background-size: 100% auto;
			}
			.the_catalog_img_2 {
				height: 200px;
				max-height: 200px;
				background-image: url(images/new_training5.jpg);
				background-size: 100% auto;
			}
			.the_catalog_img_3 {
				height: 200px;
				max-height: 200px;
				background-image: url(images/new_talent1.png);
				background-size: 100% auto;
			}
			.fa-chevron-circle-right {
				color: #77C53E;
			}
			.the_catalog_content {
				background-color: #fff;
				max-height: 200px;
				min-height: 200px;
			}
			.the_catalog_content_padding {
				height: 150px;
				max-height: 150px;
				padding: 20px;
				background-color: #fff;
			}

			* CROUSAL-STARTS */
			#sg-carousel {
			    position: relative;
			    height: 100vh;
			}
			.carousel-caption{top:50%;}
			.carousel-caption h1 {
			    font-size: 6em;
			    font-weight: bold;
			    margin: 0;
			    padding: 0;
			}
			#sg-carousel .carousel-control.left {
			    top: 35%;
			}
			#sg-carousel .carousel-control.right {
			    top: 35%;
			}
			.uppr-txt {
			    text-transform: uppercase;
			    color: #fff;
			    font-size: 35px;
			}
			#sg-carousel .carousel-control.left,
			#sg-carousel .carousel-control.right {
			    background-image: none;
			    background-repeat: no-repeat;
			    opacity: 0;
			    text-shadow: none;
			    transition: all 0.8s ease 0s;
			}
			.carousel-control {
			    bottom: 0;
			    color: #fff;
			    font-size: 20px;
			    left: 0;
			    opacity: 0;
			    position: absolute;
			    text-align: center;
			    text-shadow: 0 1px 2px rgba(0, 0, 0, 0.6);
			    top: 0;
			    width: auto;
			    transition: all 0.8s ease 0s;
			}
			
			.fa-angle-right,
			.fa-angle-left {
			    font-size: 80px;
			}
			#sg-carousel:hover .carousel-control.left {
			    left: 38px;
			    transition: all .2s ease 0;
			}
			#sg-carousel:hover .carousel-control.right {
			    right: 38px;
			    transition: all .2s ease 0;
			}
			#sg-carousel:hover .carousel-control.left,
			#sg-carousel:hover .carousel-control.right {
			    opacity: 1;
			}
			.carousel-inner > .item > a > img,
			.carousel-inner > .item > img,
			.img-responsive,
			.thumbnail a > img,
			.thumbnail > img {
			    display: block;
			    height: auto;
			    max-width: 100%;
			    width: 100%;
			}
			.item.active img {
			    transition: transform 5000ms linear 0s;
			    transform: scale(1.05, 1.50);
			}
			.carousel-fade .carousel-inner .active {
			    opacity: 1;
			}
			.carousel-fade .carousel-inner .next.left,
			.carousel-fade .carousel-inner .prev.right {
			    opacity: 1;
			}
			.carousel-fade .carousel-control {
			    z-index: 2;
			    font-size: 80px;
			}
			.carousel-caption {
			    left: 60%;
			    padding-bottom: 5%;
			    right: 0;
			    text-align: left;
			}

			.carousel-caption h1 {
			    font-family: raavi;
			    font-size: 60px;
			    font-weight: 600;
			    line-height: 18px;
			}
			.carousel-caption > p {
			    font-size: 30px;
			    color: #fff;
			}
			.carousel-caption > a {
			    text-transform: uppercase;
			    color: #fff;
			    background: #041132;
			    padding: 6px 12px;
			}
			.button--tamaya {
			    border: 2px solid #40a304 !important;
			    border-radius: 5px;
			    color: #7986cb;
			    min-width: 180px;
			    overflow: hidden;
			    text-transform: uppercase;
			    letter-spacing: 1px;
			}
			.button--border-thick {
			    border: 3px solid;
			}
			.button {
			    background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
			    border: medium none;
			    color: inherit;
			    display: block;
			    float: left;
			    max-width: 250px;
			    min-width: 199px;
			    padding: 1em 7em;
			    position: relative;
			    vertical-align: middle;
			    z-index: 1;
			}
			.button--tamaya::before {
			    padding-top: 1em;
			    top: 0;
			}
			.button--tamaya::before,
			.button--tamaya::after {
			    background: #40a304;
			    color: #fff;
			    content: attr(data-text);
			    height: 50%;
			    left: 0;
			    overflow: hidden;
			    position: absolute;
			    transition: transform 0.3s cubic-bezier(0.2, 1, 0.3, 1) 0s;
			    width: 100%;
			}
			.button--tamaya::after {
			    bottom: 0;
			    line-height: 0;
			}
			.button--tamaya:hover::before {
			    transform: translate3d(0px, -100%, 0px);
			}
			.button--tamaya:hover::after {
			    transform: translate3d(0px, 100%, 0px);
			    color: #40a304;
			}
			.button--tamaya:hover {
			    color: #40a304;
			    font-weight: 600;
			}
			.carousel-indicators li {
			    background-color: #b3b5b9;
			    border-radius: 10px;
			    cursor: pointer;
			    display: inline-block;
			    height: 12px;
			    margin: 1px;
			    text-indent: -999px;
			    width: 12px;
			    border: 0;
			}
			.carousel-indicators .active {
			    background-color: #041132;
			    height: 12px;
			    margin: 0;
			    width: 12px;
			    border: 0;
			}
			#sg-carousel h1 {
			    animation-delay: 1s;
				margin-bottom:25px;
			}
			#sg-carousel p {
			    animation-delay: 2s;
			}
			#sg-carousel button {
			    animation-delay: 3s;
				margin-top:25px;
			}
			@media all and (transform-3d),
			(-webkit-transform-3d) {
			    .carousel-fade .carousel-inner > .item.next,
			    .carousel-fade .carousel-inner > .item.active.right {
			        opacity: 0;
			        -webkit-transform: translate3d(0, 0, 0);
			        transform: translate3d(0, 0, 0);
			    }
			    .carousel-fade .carousel-inner > .item.prev,
			    .carousel-fade .carousel-inner > .item.active.left {
			        opacity: 0;
			        -webkit-transform: translate3d(0, 0, 0);
			        transform: translate3d(0, 0, 0);
			    }
			    .carousel-fade .carousel-inner > .item.next.left,
			    .carousel-fade .carousel-inner > .item.prev.right,
			    .carousel-fade .carousel-inner > .item.active {
			        opacity: 1;
			        -webkit-transform: translate3d(0, 0, 0);
			        transform: translate3d(0, 0, 0);
			    }
			}
		</style>
	</head>

	<body>
	<?php
        include ("header_bottom.php");
    ?>

	<?php
        include ("header.php");
    ?>

	<div class="">
  		<div class="container-fluid">
			<div class="row">
				<div id="sg-carousel" class="carousel slide carousel-fade" data-ride="carousel">
					<ol class="carousel-indicators">
						<li data-target="#carousel" data-slide-to="0" class="active"></li>
						<li data-target="#carousel" data-slide-to="1" class=""></li>
						<li data-target="#carousel" data-slide-to="2" class=""></li>
					</ol>
					<!-- Carousel items -->
					<div class="carousel-inner carousel-zoom">
						<div class="item active">
					        <img src="images/people3.jpg" alt="Los Angeles" style="width:100%;height: 600px;">
					        <div class="carousel-caption">
					        	<h3 class="animated fadeInLeft" style="font-family: montserratReg; position: absolute; right: 460px; bottom: 255px; color: #fff; font-size: 100px; text-transform: uppercase;">people</h3>
				        	</div>
					    </div>

					    <div class="item">
					        <img src="images/advisory.png" alt="Chicago" style="width:100%;height: 600px;">
					        <div class="carousel-caption">
					          <h3 class="animated slideInDown" style="font-family: montserratReg; position: absolute; right: 480px; color: #fff; bottom: 255px; font-size: 100px; text-transform: uppercase;">ideas</h3>
			        		</div>
				      	</div>
				    
				      	<div class="item">
					        <img src="images/three_3.jpg" alt="New york" style="width:100%;height: 600px;">
					        <div class="carousel-caption">
					          <h3 class="animated flip" style="font-family: montserratReg; position: absolute; right: 370px; color: #fff; bottom: 255px; font-size: 100px; text-transform: uppercase;">strategy</h3>
				        	</div>
				      	</div>
						<!-- Carousel nav -->
						<a class="carousel-control left" href="#sg-carousel" data-slide="prev">‹</a>
						<a class="carousel-control right" href="#sg-carousel" data-slide="next">›</a>
					</div> 
				</div>
			</div>
		</div>
	</div>

	<div class="banner-bottom" style="background-color: #F2F2F2;">
		<div class="container">
			<div class="row">
				<div class="col-md-4 the_catalog_img_parent">
					<div class="the_catalog_img_1"></div>
					<div class="the_catalog_content">
						<h1 style="text-align: center; padding: 10px; font-weight: 600; color: #251021; font-family: montserratReg; font-size: 25px;">Training</h1>
						<p style="text-align: center; font-family: opensans; color: #777777; margin-bottom: 30px;">
							We work with organisations to make the workplace "suicide safer". Depending on our clients' need, our services entail making presentation and organizing training...
						</p>
						<p style="text-align: center; font-size: 15px;">
							<a href="training.php" style="font-weight: 600; font-family: opensans; color: #777777;">
								read more
								<i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
							</a>
						</p>
					</div>
				</div>
				<div class="col-md-4 the_catalog_img_parent">
					<div class="the_catalog_img_2"></div>
					<div class="the_catalog_content">
						<h1 style="text-align: center; padding: 10px; font-weight: 600; color: #251021; font-family: montserratReg; font-size: 25px;">Advisory</h1>
						<p style="text-align: center; font-family: opensans; color: #777777; margin-bottom: 30px;">
							At Jesshill Consulting, we are committed to helping our clients make discoveries of actionable insights on their pressing business issues... 
						</p>
						<p style="text-align: center; font-size: 15px;">
							<a href="advisory.php" style="font-weight: 600; font-family: opensans; color: #777777;">
								read more
								<i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
							</a>
						</p>
					</div>
				</div>
				<div class="col-md-4 the_catalog_img_parent">
					<div class="the_catalog_img_3"></div>
					<div class="the_catalog_content">
						<h1 style="text-align: center; padding: 10px; font-weight: 600; color: #251021; font-family: montserratReg; font-size: 25px;">Talent Hunt</h1>
						<p style="text-align: center; color: #777777; font-family: opensans; margin-bottom: 30px;">
							We are committed to collaborating with our clients at finding great talents. We employ high level expertise to the task of finding the best candidate for a vacancy. Our hiring decisions...
						</p>
						<p style="text-align: center; font-size: 15px;">
							<a href="talent.php" style="font-weight: 600; font-family: opensans; color: #777777;">
								read more
								<i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
							</a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- //banner-bottom -->

	<!-- testimonials -->
		<div class="testimonials"> 
			<div class="container">
		   		<h3 class="w3l_header two" style="color: #fff; text-transform: uppercase; font-size: 36px; font-weight: 600; font-family: montserratReg;">WHY CLIENTS CHOOSE US</h3>
				<p style="text-align: center; width: 3%; margin: auto; height: 7px; background-color: #77C53E; margin-bottom: 5em;"></p>
				<div class="row">
					<div class="col-md-4 the_catalog_content_parent">
						<div class="the_catalog_content_padding">
							<h1 style="text-align: center; padding-bottom: 10px; font-weight: 600; color: #251021; font-size: 25px; font-family: montserratReg;">Client Satisfaction</h1>
							<p style="text-align: center; font-family: opensans; color: #777777;">
								We proactively respond to our clients' needs and collaborate with them to achieve their business goals.
							</p>
						</div>
					</div>
					<div class="col-md-4 the_catalog_content_parent">
						<div class="the_catalog_content_padding">
							<h1 style="text-align: center; padding-bottom: 10px; font-weight: 600; color: #251021; font-size: 25px; font-family: montserratReg;">Excellence</h1>
							<p style="text-align: center; font-family: opensans; color: #777777;">
								We strive to excel in every aspect of our business and approach tasks with a determination to succeed and make a lasting impact.
							</p>
						</div>
					</div>
					<div class="col-md-4 the_catalog_content_parent">
						<div class="the_catalog_content_padding">
							<h1 style="text-align: center; padding-bottom: 10px; font-weight: 600; color: #251021; font-size: 25px; font-family: montserratReg;">Integrity</h1>
							<p style="text-align: center; font-family: opensans; color: #777777;">
								 We are guided by and uphold high level of professionalism in our daily interaction. 
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	<!-- //testimonials -->

	<?php
        include ("footer.php");
    ?>

	<!-- js -->
		<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
		<!-- carousal -->
			<script src="js/slick.js" type="text/javascript"></script>
			<script type="text/javascript">
				$(document).on('ready', function() {
				  $(".center").slick({
					dots: true,
					infinite: true,
					centerMode: true,
					slidesToShow: 2,
					slidesToScroll: 2,
					responsive: [
						{
						  breakpoint:800,
						  settings: {
							arrows: true,
							centerMode: false,
							slidesToShow: 1
						  }
						},
						{
						  breakpoint: 480,
						  settings: {
							arrows: true,
							centerMode: false,
							centerPadding: '40px',
							slidesToShow: 1
						  }
						}
					 ]
				  });
				});
			</script>
		<!-- //carousal -->
	<!-- //js -->

	<script>
	  $(function(){ 

	  // parameters
	  // image height
	  var images_height = '650px';
	  // array of images
	  var images_url = [
	      'images/1.jpg',
	      'images/2.jpg',
	      'images/3.jpg'
	  ];
	  var images_count = images_url.length;

	  // create nodes
	  for(var j=0;j<images_count+1;j++){
	      $('.banner ul').append('<li></li>')
	  }

	  // pagination
	  for(var j=0;j<images_count;j++){
	      if(j==0){
	          $('.banner ol').append('<li class="current"></li>')
	      }else{
	          $('.banner ol').append('<li></li>')
	      }
	  }

	  // convert images into backgrounds
	  $('.banner ul li').css('background-image','url('+images_url[0]+')');
	  
	  $.each(images_url,function(key,value){
	      $('.banner ul li').eq(key).css('background-image','url('+value+')');
	  });

	  $('.banner').css('height',images_height);

	  $('.banner ul').css('width',(images_count+1)*100+'%');

	  $('.banner ol').css('width',images_count*20+'px');
	  $('.banner ol').css('margin-left',-images_count*20*0.5-10+'px');

	  var num = 0;

	  var window_width = $(window).width();

	  $(window).resize(function(){
	      window_width = $(window).width();
	      $('.banner ul li').css({width:window_width});
	      clearInterval(timer);
	      nextPlay();
	      timer = setInterval(nextPlay,6000);
	  });

	  $('.banner ul li').width(window_width);

	  // pagination dots
	  $('.banner ol li').mouseover(function(){
	      $(this).addClass('current').siblings().removeClass('current');
	      var i = $(this).index();
	      //console.log(i);
	      $('.banner ul').stop().animate({left:-i*window_width},500);
	      num = i;
	  });

	  // autoplay
	  var timer = null;

	  function prevPlay(){
	      num--;
	      if(num<0){
	          $('.banner ul').css({left:-window_width*images_count}).stop().animate({left:-window_width*(images_count-1)},500);
	          num=images_count-1;
	      }else{
	          $('.banner ul').stop().animate({left:-num*window_width},500);
	      }
	      if(num==images_count-1){
	          $('.banner ol li').eq(images_count-1).addClass('current').siblings().removeClass('current');
	      }else{
	          $('.banner ol li').eq(num).addClass('current').siblings().removeClass('current');

	      }
	  }

	  function nextPlay(){
	      num++;
	      if(num>images_count){
	          $('.banner ul').css({left:0}).stop().animate({left:-window_width},500);
	          num=1;
	      }else{
	          $('.banner ul').stop().animate({left:-num*window_width},500);
	      }
	      if(num==images_count){
	          $('.banner ol li').eq(0).addClass('current').siblings().removeClass('current');
	      }else{
	          $('.banner ol li').eq(num).addClass('current').siblings().removeClass('current');

	      }
	  }

	  timer = setInterval(nextPlay,6000);

	  // auto pause on mouse enter
	  $('.banner').mouseenter(function(){
	      clearInterval(timer);
	      $('.banner i').fadeIn();
	  }).mouseleave(function(){
	      timer = setInterval(nextPlay,6000);
	      $('.banner i').fadeOut();
	  });

	  // goto next
	  $('.banner .right').click(function(){
	      nextPlay();
	  });

	  // back to previous
	  $('.banner .left').click(function(){
	      prevPlay();
	  });

	});
	    </script>


	<!-- flexisel -->
			<script type="text/javascript">
			$(window).load(function() {
				$("#flexiselDemo1").flexisel({
					visibleItems: 4,
					animationSpeed: 1000,
					autoPlay: true,
					autoPlaySpeed: 3000,    		
					pauseOnHover: true,
					enableResponsiveBreakpoints: true,
					responsiveBreakpoints: { 
						portrait: { 
							changePoint:480,
							visibleItems: 1
						}, 
						landscape: { 
							changePoint:640,
							visibleItems:2
						},
						tablet: { 
							changePoint:768,
							visibleItems: 2
						}
					}
				});
				
			});
		</script>
		<script type="text/javascript" src="js/jquery.flexisel.js"></script>
	<!-- //flexisel -->
	<!-- odometer-script -->
				<script src="js/odometer.js"></script>
				<script>
					window.odometerOptions = {
					  format: '(,ddd).dd'
					};
					setTimeout(function(){
						jQuery('#w3l_stats1').html(25);
					}, 1000);
				</script>
				<script>
					window.odometerOptions = {
					  format: '(,ddd).dd'
					};
					setTimeout(function(){
						jQuery('#w3l_stats2').html(330);
					}, 1000);
				</script>
				<script>
					window.odometerOptions = {
					  format: '(,ddd).dd'
					};
					setTimeout(function(){
						jQuery('#w3l_stats3').html(22496);
					}, 1000);
				</script>
				<script>
					window.odometerOptions = {
					  format: '(,ddd).dd'
					};
					setTimeout(function(){
						jQuery('#w3l_stats4').html(620);
					}, 1000);
				</script>
			<!-- //odometer-script -->
	<!-- start-smoth-scrolling -->
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
	</script>
	<!-- start-smoth-scrolling -->
		

	<!-- here stars scrolling icon -->
		<script type="text/javascript">
			$(document).ready(function() {
				/*
					var defaults = {
					containerID: 'toTop', // fading element id
					containerHoverID: 'toTopHover', // fading element hover id
					scrollSpeed: 1200,
					easingType: 'linear' 
					};
				*/
									
				$().UItoTop({ easingType: 'easeOutQuart' });
									
				});
		</script>
	<!-- //here ends scrolling icon -->
	<!-- for bootstrap working -->
		<script src="js/bootstrap.js"></script>
	<!-- //for bootstrap working -->















	<!-- <div id="myCarousel" class="carousel slide" data-ride="carousel">
			    <ol class="carousel-indicators">
			      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			      <li data-target="#myCarousel" data-slide-to="1"></li>
			      <li data-target="#myCarousel" data-slide-to="2"></li>
			    </ol>
		    <div class="carousel-inner">
		      	<div class="item active">
			        <img src="images/1.jpg" alt="Los Angeles" class="animated fadeInRight" style="width:100%;height: 600px;">
			        <div class="carousel-caption">
			          <h3 class="h3 animated fadeInLeft" style="font-family: montserratReg;">people</h3>
		        	</div>
			    </div>

			    <div class="item">
			        <img src="images/advisory.png" alt="Chicago" class="animated slideInUp" style="width:100%;height: 600px;">
			        <div class="carousel-caption">
			          <h3 class="h3 animated slideInDown" style="font-family: montserratReg;">ideas</h3>
	        		</div>
		      	</div>
		    
		      	<div class="item">
			        <img src="images/3.jpg" alt="New york" class="animated rotateIn" style="width:100%;height: 600px;">
			        <div class="carousel-caption">
			          <h3 class="h3 animated flip" style="font-family: montserratReg;">strategy</h3>
		        	</div>
		      	</div>
    		</div>
		    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
		      <i class="fa fa-angle-left con" aria-hidden="true" ></i>
		      <span class="sr-only">Previous</span>
		    </a>
		    <a class="right carousel-control" href="#myCarousel" data-slide="next">
		      <i class="fa fa-angle-right con" aria-hidden="true"></i>
		      <span class="sr-only">Next</span>
		    </a>
  		</div> -->
	</body>
</html>