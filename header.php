<div class="header" id="home" style="background-color: #fff;">
	<div class="container">
		<div class="header-main">
			<div class="col-md-6 header-left">
				<a href="index.php">
					<img src="images/logo_1.png" style="height: 80px; width: 50%;">
				</a>
			</div>
			<div class="col-md-6 header-right">
				<ul>
					<li>
						<a href="https://www.facebook.com/JesshillConsulting/" target="_blank">
							<i class="fa fa-facebook-square socal" aria-hidden="true"></i>
						</a>
					</li>
					<li>
						<a href="https://twitter.com/jesshillconsult" target="_blank">
							<i class="fa fa-twitter-square socal" aria-hidden="true"></i>
						</a>
					</li>
					<li>
						<a href="https://www.linkedin.com/in/jesshill-consulting-59ab3b146/?trk=uno-choose-ge-no-intent&dl=no" target="_blank">
							<i class="fa fa-linkedin-square socal soc" aria-hidden="true"></i>
						</a>
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!--header-ends--> 