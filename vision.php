<!doctype html>
<html>
	<head>
		<title>Jesshill | About Us</title>
		<!-- custom-theme -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="Oil Industry Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
		Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
				function hideURLbar(){ window.scrollTo(0,1); } </script>
		<!-- //custom-theme -->
		<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
		<link rel="stylesheet" href="css/style.css">
		<link href="css/stylee.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/test.css" rel="stylesheet" type="text/css" media="all" />
		<link rel="stylesheet" type="text/css" href="css/fonts/font.css">
		<!-- font-awesome-icons -->
		<link href="css/font-awesome.css" rel="stylesheet"> 
		<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
		<script src="js/bootstrap.js"></script>
		<style>
			.mySlides {display:none;height: 600px;}
			.dropdown-menu a:hover {background-color: #f1f1f1;}
			.dropdown:hover .dropdown-menu {
			    display: block;
			}
			.dropdown-menu a {
				text-transform: uppercase;
			}
			.left_div_for_image {
				float: left;
			    height: 50px;
			    width: 5%;
			}
			.right_div_for_content {
			    float: right;
			    width: 94%;
			    padding-top: 10px;
			    font-family: opensans;
			    color: #777777;
			}
			.parent {
				margin-bottom: 25px;
			}
		</style>
	</head>
	<body>
		<?php
	        include ("header_bottom.php");
	    ?>
	    
		<?php
	        include ("header.php");
	    ?>

	    <section class="banner-w3ls2">
			<div class="container">
				<h1 class="text-center agileits-w3layouts agile w3-agile" style="color: #B0CE2D; font-weight: 600; margin-top: 150px !important;">
				</h1>
			</div>
		</section>
		<section class="info-w3ls2">
			<div class="container">
				<h3 class="text-center agileits-w3layouts agile w3-agile" style="padding: 0px;">Our Vision and Mission</h3>
				<p style="text-align: center; width: 3%; margin: auto; height: 7px; background-color: #77C53E; margin-bottom: 2em;"></p>
				<p class="text-center" style="text-align: justify; color: #777777; font-size: 15px; font-family: opensans;">At Jesshill Consulting, our <b>VISION</b> is to be the leading consulting firm of choice in Sub-Sahara Africa. <br> <br> Our <b>MISSION</b> is to create and deliver ideas and strategies that will help our clients make distinctive and lasting improvement in the bottom-line of their organisations while abiding by the utmost ethical standards.
			</div>
			<div class="container">
				<h3 class="text-center agileits-w3layouts agile w3-agile" style="padding: 20px 0px 0px 0px;">CORE VALUES</h3>
				<p style="text-align: center; width: 3%; margin: auto; height: 7px; background-color: #77C53E; margin-bottom: 2em;"></p>
				<p style="font-size: 15px; color: #777777; font-family: opensans;">
					We are driven by the following <b>CORE VALUES</b>:
				</p>
				<br>
				<div class="parent">
					<div class="left_div_for_image">
						<img style="height: 50px;" src="images/trusticon.gif">						
					</div>
					<div class="right_div_for_content">
						<b>Trust</b>- We earn the trust of our clients by collaborating with them towards efficiency in their work engagements
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>

				<div class="parent">
					<div class="left_div_for_image">
						<img style="height: 50px;" src="images/honest_icon.png">						
					</div>
					<div class="right_div_for_content">
						<b>Integrity</b>- We abide by ethical standards in the discharge of our duty and encourage our clients to walk the same path
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>

				<div class="parent">
					<div class="left_div_for_image">
						<img style="height: 50px;" src="images/Idea-512.png">						
					</div>
					<div class="right_div_for_content">
						<b>Creativity</b>- Stereotype sucks. We constantly encourage innovative and out of the box ideas.
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>

				<div class="parent">
					<div class="left_div_for_image">
						<img style="height: 50px;" src="images/knowledge.png">						
					</div>
					<div class="right_div_for_content">
						<b>Knowledge-</b> We are constantly improving our knowledge and encourage same for our clients. We believe that knowledge is progressive.
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
		</section>
		<section class="banner-w3ls3">
			<div class="container">
				<h3 class="text-center">Vision is the act of seeing what is invisible to others</h3>
			</div>
		</section>
		<?php
	        include ("footer.php");
	    ?>
	</body>
</html>