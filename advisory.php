<!DOCTYPE html>
<html lang="en">
	<head>
		<title></title>
		<!-- custom-theme -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="Driving School Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
		Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
				function hideURLbar(){ window.scrollTo(0,1); } </script>
		<!-- //custom-theme -->
		<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/whatwedostyle.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/stylee.css" rel="stylesheet" type="text/css" media="all" />
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<link rel="stylesheet" type="text/css" href="css/fonts/font.css">
		<link rel="stylesheet" href="css/test.css">
		<!-- js -->
		<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
		<!-- //js -->
  		<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
		<link href="css/font-awesome.css" rel="stylesheet"> 
		<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" property="" />
		<!-- //font-awesome-icons -->
	</head>
	<style>
		.mySlides {display:none;height: 600px;}
		.dropdown-menu a:hover {background-color: #f1f1f1;}
		.dropdown:hover .dropdown-menu {
		    display: block;
		}
		.dropdown-menu a {
			text-transform: uppercase;
		}
		.float_left {
			float: left;
			width: 10%;
		}
		.float_right {
			float: right;
			padding-left: 20px;
			width: 90%;
		}
		.fa_square {
			font-size: 40px;
		    color: #fff;
		    width: 90%;
		    background: #251021;;
		    height: 50%;
		    text-align: center;
		    border-radius: 30%;
		    margin-right: 20px;
		}
		.float_right_h1 {
			font-size: 18px;
			text-transform: capitalize;
			font-family: montserratReg;
			margin-bottom: 15px;
			font-weight: 600;
		}
	</style>
	<body>
		<?php
	        include ("header_bottom.php");
	    ?>

		<?php
	        include ("header.php");
	    ?>

		<div class="w3-container">
	 
		</div>

		<div class="">
	  		<div id="myCarousel" class="carousel slide" data-ride="carousel">
	    		<div class="carousel-inner">
			      	<div class="item active">
				        <img src="images/new_training5.jpg" alt="Los Angeles" style="width:100%;height: 600px;">
				        <div class="carousel-caption">
				          <h1 style="color: #fff; text-align: center; font-weight: 600; margin-bottom: 200px!important; font-size: 100px; font-family: montserratReg;">ADVISORY</h1>
				        </div>
				    </div>

			      	<div class="item">
				        <img src="images/new_advisor.jpg" alt="Chicago" style="width:100%;height: 600px;">
				        <div class="carousel-caption">
				          <h1 style="color: #fff; font-weight: 600; margin-bottom: 200px!important; font-size: 100px; font-family: montserratReg;"> CONSULTATIVE</h1>
				        </div>
				    </div>

				    <div class="item">
				        <img src="images/recomend.jpg" alt="Chicago" style="width:100%;height: 600px;">
				        <div class="carousel-caption" style="left: 10% !important;">
				          <h1 style="color: #fff; font-weight: 600; margin-bottom: 200px!important; font-size: 100px; font-family: montserratReg;"> RECOMMENDATION</h1>
				        </div>
				    </div>
	    		</div>
	  		</div>
	  	</div>
		
		<div class="">
			<div class="div" style="background-color: #F2F2F2; padding: 30px;">
				<div class="container">
					<div class="col-md-12 w3layouts_register_right">
						<h1 style="text-align: center; font-size: 42px; font-family: montserratReg;">
							ADVISORY
						</h1>
						<p style="text-align: center; width: 3%; margin: auto; height: 7px; background-color: #77C53E; margin-bottom: 2em;"></p>

						<p style="color: #5F5F5F; font-size: 15px; line-height: 30px; margin-bottom: 20px; text-align: justify;">
							At Jesshill Consulting, we are committed to helping our clients make discoveries of actionable insights on their pressing business issues. We seek to address the most complex and interesting opportunities which are imperative to business success. 
							Our business advisory team develop innovative solutions for specific client business needs. Our main objective of offering business advisory services is to bring more focus to value propositions. We create value for our clients to enable them the opportunity to thrive in a competitive business landscape. Our services can support you wherever you are — whether you’re looking at a transaction to propel you forward, focusing on developing and implementing the right controls to mitigate risk, or advancing your company’s finance and technology infrastructure to match your aspirations. <br><br>
							We deliver our advisory services in a way that ensures that value is created, protected and transformed.
						</p>

						<div class="container">
							<div class="row">
								<div class="col-md-6">
									<div class="float_left">
										<i class="fa fa-pencil fa_square" aria-hidden="true"></i>
									</div>
									<div class="float_right">
										<h1 class="float_right_h1">
											Transaction services to create value
										</h1>
										<p style="font-size: 15px; font-family: opensans; color: #777777; line-height: 30px; text-align: justify;">
											We advise new ventures (when required) for competitive advantages and business profitability through diligence, corporate finance and operations.
										</p>
									</div>
								</div>
								<div class="col-md-6">
									<div class="float_left">
										<i class="fa fa-lock fa_square" aria-hidden="true"></i>
									</div>
									<div class="float_right">
										<h1 class="float_right_h1">
											Business risk services to protect value:
										</h1>
										<p style="font-size: 15px; font-family: opensans; color: #777777; line-height: 30px; text-align: justify;">
											We identify risks and advise organisations to manage and mitigate these risks through controls, regulatory compliance, and forensics and data analytics.
										</p>
									</div>
								</div>
							</div>

							<div class="row" style="margin-top: 30px;">
								<div class="col-md-6">
									<div class="float_left">
										<i class="fa fa-check-circle fa_square" aria-hidden="true"></i>
									</div>
									<div class="float_right">
										<h1 class="float_right_h1">
											Business consulting & technology to transform value:
										</h1>
										<p style="font-size: 15px; font-family: opensans; color: #777777; line-height: 30px; text-align: justify;">
											We advise optimum financial management strategy ,performance improvement strategy, technology strategies and solutions. 
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>

		<section class="banner-w3ls32">
			<div class="container">
			</div>
		</section>		
		

		<?php
	        include ("footer.php");
	    ?>

	<!-- for bootstrap working -->
		<script src="js/bootstrap.js"></script>
	<!-- //for bootstrap working -->
	<!-- here stars scrolling icon -->
		<script type="text/javascript">
			$(document).ready(function() {
				/*
					var defaults = {
					containerID: 'toTop', // fading element id
					containerHoverID: 'toTopHover', // fading element hover id
					scrollSpeed: 1200,
					easingType: 'linear' 
					};
				*/
									
				$().UItoTop({ easingType: 'easeOutQuart' });
									
				});
		</script>
		<script>
			var myIndex = 0;
			carousel();

			function carousel() {
			    var i;
			    var x = document.getElementsByClassName("mySlides");
			    for (i = 0; i < x.length; i++) {
			       x[i].style.display = "none";  
			    }
			    myIndex++;
			    if (myIndex > x.length) {myIndex = 1}    
			    x[myIndex-1].style.display = "block";  
			    setTimeout(carousel, 3000); // Change image every 2 seconds
			}
		</script>
		<script>
	            var leftOffset = 0;
	            var moveHeading = function () {
	           
	            $("#heading").offset({ left: leftOffset });
	            leftOffset++;
	            if (leftOffset > 1200) {
	            leftOffset = 0;
	            }
	            };
	            setInterval(moveHeading, 30);
	        </script>
	<!-- //here ends scrolling icon -->
	</body>
</html>