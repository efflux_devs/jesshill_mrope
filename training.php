<!DOCTYPE html>
<html lang="en">
	<head>
		<title></title>
		<!-- custom-theme -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="Driving School Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
		Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
				function hideURLbar(){ window.scrollTo(0,1); } </script>
		<!-- //custom-theme -->
		<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/whatwedostyle.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/stylee.css" rel="stylesheet" type="text/css" media="all" />
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<link rel="stylesheet" type="text/css" href="css/fonts/font.css">
		<link rel="stylesheet" href="css/test.css">
		<link rel="stylesheet" type="text/css" href="css/training.css">
		<!-- js -->
		<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
		<!-- //js -->
  		<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
		<link href="css/font-awesome.css" rel="stylesheet"> 
		<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" property="" />
		<!-- //font-awesome-icons -->
	</head>
	<style>
		.mySlides {display:none;height: 600px;}
		.dropdown-menu a:hover {background-color: #f1f1f1;}
		.dropdown:hover .dropdown-menu {
		    display: block;
		}
		.dropdown-menu a {
			text-transform: uppercase;
		}
		.side_contact {
			width: 90%;
			height: auto;
			background-color: #77C33E;
			padding: 20px 40px;
		}

		.button {
			text-align: center;
			width: 80%;
		    margin: auto;
		    display: block;
		    text-align: center;
		    color: #777777;
		    cursor: pointer;
		    font-weight: 600;
		    font-family: montserratReg;
		    font-size: 20px;
		}
		.button:hover {
			color: #251021;
		}

		button.accordion {
		    background-color: #eee;
		    color: #251021;
		    cursor: pointer;
		    padding: 18px;
		    font-family: montserratReg;
		    width: 100%;
		    border: none;
		    text-align: left;
		    outline: none;
		    font-size: 15px;
		    text-transform: uppercase;
		    transition: 0.4s;
		}

		button.accordion.active, button.accordion:hover {
		    background-color: #ddd;
		}

		button.accordion:after {
		    content: '\002B';
		    color: #777;
		    font-weight: bold;
		    float: right;
		    margin-left: 5px;
		}

		button.accordion.active:after {
		    content: "\2212";
		}

		div.panel {
		    padding: 0 18px;
		    background-color: #F2F2F2;
		    max-height: 0;
		    line-height: 30px;
		    overflow: hidden;
		    transition: max-height 0.2s ease-out;
		}
	</style>
<body>
	<?php
        include ("header_bottom.php");
    ?>

	<?php
        include ("header.php");
    ?>

	<div class="w3-container">
 
	</div>

	<div class="">
  		<div id="myCarousel" class="carousel slide" data-ride="carousel">
    		<div class="carousel-inner">
		      <div class="item active">
		        <img src="images/training_again.jpg" alt="Chicago" style="width:100%;height: 600px;">
		        <div class="carousel-caption">
		          <h1 style="color: #fff; font-weight: 600; margin-bottom: 200px!important; font-size: 100px; font-family: montserratReg;">LEARNING</h1>
		        </div>
		      </div>
    			
		      <div class="item">
		        <img src="images/image.jpg" alt="New York" style="width:100%;height: 600px;">
		        <div class="carousel-caption">
		          <h1 style="color: #fff; font-weight: 600; margin-bottom: 200px!important; font-size: 100px; font-family: montserratReg;">
		          	BUILDING
		          </h1>
		        </div>
		      </div>
    		</div>
  		</div>
  	</div>


	<div class="banner-bottom-icons">
		<div class="">
			<div class="col-md-12 w3_banner_bottom_icons_right" style="padding: 0px 0px 20px 0px;background-color: #fff; font-family: opensans;">
				<div class="container">
					
					<p style="color: #777777; padding: 20px 20px; font-size: 15px; line-height: 30px; text-align: justify;">
						We work with organisations to make the workplace "suicide safer". Depending on our client's need, our services entail making presentation and organizing training programs. Our training and interventions are easily transferrable to the workplace. We aim to strike a balance between presenting essential background information and facilitating participative exercises that are both challenging and motivating thereby enabling experiential learning to take place. <br> <br> Our training have become known for enabling learning through the exchange of experiences and ideas, facilitating motivating practical exercises to consolidate learning as well as presenting key ideas in straight forward and memorable ways. <br> 
					</p>

					<p style="font-size: 15px; padding: 20px 20px 20px 10px; background-color: #5D5D5D; margin-left: 20px; border-left: 10px solid #77C53E; color: #fff;">
						<em>
							At Jesshill Consulting, we are committed to supporting our clients’ sustainable growth. This commitment is the vital source from which our energy, style and values manifests themselves.
						</em>
					</p>

					<p style="font-size: 15px; padding: 20px 20px; line-height: 30px; color: #777777;">
						Our training services are grouped under seven broad programs. They may be general or bespoke depending on requirements and gaps in specific organisations under review. <br>
						These programs are:
					</p>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="whitebackgroundbody">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<div style="padding: 20px;">
							<button class="accordion">Customer Service Excellence/ Relationship Management Program</button>
							<div class="panel">
							  <p style="font-size: 15px; color: #777777; text-align: justify;">We know that successful businesses are clear about what their customers want and structure their organisations to deliver. For these organisations, service excellence is a major priority.
								Our expertise supports this holistic approach and combines research, consultancy, training and measurement to help our clients develop sustainable customer service strategies that meet their customers’ expectations. Our approach delivers real improvements for our clients in terms of commercial metrics such as sales conversion, operating costs and staff attrition. Of importance to us are the following: <br>
								<b>Customers value:</b> we understand what matters to customers by uncovering what is impacting current service performance – what is getting in the way of meeting or exceeding customers’ expectations – and we use our knowledge to challenge management thinking and practices),<br>
								<b>Staff engagement:</b> Customers have interface with employees of organisations. These engagements are in part responsible for repeat purchase/ patronage. Bearing this in mind, we engage front line staff in defining what customer service should be like and uncover what they thinks gets in the way.<br>
								<b>Measuring service performance:</b> We help our clients to develop measurement systems that track customer service, customer satisfaction in a way that can drive service performance and improvement.</p>
							</div>

							<button class="accordion">Leadership Development and Management Program</button>
							<div class="panel">
							  	<p style="font-size: 15px; color: #777777; text-align: justify;">
							  		Our Leadership Development and Management Training Program helps organizations understand what capabilities they will need to succeed in an ever changing and complex corporate world. We provide trainings that match our clients’ organization's capabilities, producing innovative and practical solutions for the noticeable leadership gaps. In addition to this, we believe that cultivating the next generation of leaders starts with a thorough examination of what the future organization should look like.  With this in mind, participants can develop an understanding of the skills, attributes and behaviors needed to meet the organization’s goals and embark on the course of personal development.
						  		</p>
							</div>

							<button class="accordion">Sales and Marketing Development Program</button>
							<div class="panel">
							  	<p style="font-size: 15px; color: #777777; text-align: justify;">
							  		Our Sales and Marketing Development training involves but are not limited to defining suitable sales strategy, sales process, sales enablement and Analytics for organisations.
									<br>
									<b>Sales Strategy:</b> We help to determine the sales strategy that best fit our clients through proper analysis of their goals. This is in addition to refining their segmentation model.
									<br>
									<b>Sales Process:</b> We know that development of a well-defined sales process is critical to accelerate sales growth. Our job is to collaborate with our clients and define the process of why, when, and how their sales team engages with buyers<br>
									<b>Sales Analytics:</b> Measurement of achievement vis-à-vis set target, business goals are critical to ensuring superlative results. Our commitment is to determine key success measurements that can be utilized by our clients to provide real insights that will optimize their strategy and bottom line.
							  	</p>
							</div>

							<button class="accordion">Strategic Management Program</button>
							<div class="panel">
							  	<p style="font-size: 15px; color: #777777; text-align: justify;">
							  		We are aware that strategic management is critical to the health of organisations. This is because it charters a course for an organisation into the future by paying attention to current conditions, future predictions and available resources when devising the strategy. Through our Strategic Management training, we work with our clients by paying attention to the present while anticipating future occurrences and devising the right strategies to meet needs as they occur.
							  	</p>
							</div>

							<button class="accordion">Project Management Program</button>
							<div class="panel">
							  	<p style="font-size: 15px; color: #777777; text-align: justify;">
							  		Our aim is to help our clients improve their project management maturity, establishing consistent, practical project methodology and PMO functions. We provide a blended approach of consulting and training to support our clients’ goal of improved project performance.
							  	</p>
							</div>

							<button class="accordion">Business Planning and Finance Program</button>
							<div class="panel">
							  	<p style="font-size: 15px; color: #777777; text-align: justify;">
							  		A practicable and workable business plan, financial projections and pitch deck are vital tools needed for business successes. Our team is equipped with the necessary expertise at helping guide and chart a veritable financial management and business planning course for our clients using up to date tools for training.
							  	</p>
							</div>

							<button class="accordion">Motivation and Personal Development Program</button>
							<div class="panel">
							  	<p style="font-size: 15px; color: #777777; text-align: justify;">
							  		Motivation and personal development training is designed to furnish participants with essential skillset needed to function effectively by becoming better managers of themselves and interact better with others both on and off the job. It offers skills and knowledge that help employees boost personal performance and productivity. The philosophy of this training emphasizes how personal mastery impacts business performance in the workplace.
							  	</p>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div style="padding: 20px;">
							<div class="side_contact">
								<h2 style="margin-bottom: 15px; text-align: center; font-size: 20px; font-weight: 600; font-family: montserratReg; color: #251021;">How can we help you?</h2>
								<a href="contact.php" class="button">Contact Us</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="clearfix"></div>

	<div class="clearfix"></div>

	<?php
        include ("footer.php");
    ?>

<!-- for bootstrap working -->
	<script src="js/bootstrap.js"></script>
<!-- //for bootstrap working -->
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
	<script>
		var myIndex = 0;
		carousel();

		function carousel() {
		    var i;
		    var x = document.getElementsByClassName("mySlides");
		    for (i = 0; i < x.length; i++) {
		       x[i].style.display = "none";  
		    }
		    myIndex++;
		    if (myIndex > x.length) {myIndex = 1}    
		    x[myIndex-1].style.display = "block";  
		    setTimeout(carousel, 3000); // Change image every 2 seconds
		}
	</script>
	<script>
        var leftOffset = 0;
        var moveHeading = function () {
       
        $("#heading").offset({ left: leftOffset });
        leftOffset++;
        if (leftOffset > 1200) {
        leftOffset = 0;
        }
        };
        setInterval(moveHeading, 30);
    </script>
<!-- //here ends scrolling icon -->

	<script>
		var acc = document.getElementsByClassName("accordion");
		var i;

		for (i = 0; i < acc.length; i++) {
		    acc[i].onclick = function(){
		        this.classList.toggle("active");
		        var panel = this.nextElementSibling;
		        if (panel.style.display === "block") {
		            panel.style.display = "none";
		        } else {
		            panel.style.display = "block";
		        }
		    }
		}
	</script>

	<script>
		var acc = document.getElementsByClassName("accordion");
		var i;

		for (i = 0; i < acc.length; i++) {
		  acc[i].onclick = function() {
		    this.classList.toggle("active");
		    var panel = this.nextElementSibling;
		    if (panel.style.maxHeight){
		      panel.style.maxHeight = null;
		    } else {
		      panel.style.maxHeight = panel.scrollHeight + "px";
		    } 
		  }
		}
	</script>

	<script>
		var myIndex = 0;
		carousel();

		function carousel() {
		    var i;
		    var x = document.getElementsByClassName("mySlides");
		    for (i = 0; i < x.length; i++) {
		       x[i].style.display = "none";  
		    }
		    myIndex++;
		    if (myIndex > x.length) {myIndex = 1}    
		    x[myIndex-1].style.display = "block";  
		    setTimeout(carousel, 3000); // Change image every 2 seconds
		}
	</script>
</body>
</html>